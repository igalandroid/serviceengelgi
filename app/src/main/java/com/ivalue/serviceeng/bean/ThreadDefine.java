package com.ivalue.serviceeng.bean;

/**
 * Created by railsfactory on 12/7/15.
 */
public class ThreadDefine {


    public  static  final int SEND_SMS_SUCCESS =101;

    public  static  final int SEND_SMS_FAILED =102;

    public static final int GET_CUSTOMER_DETAILS = 103;

    public static final int GET_CUSTOMER_DETAILS_SUCCESS = 104;

    public static final int GET_CUSTOMER_DETAILS_FAILED = 105;

    public static final int LOGIN =106;

    public static final int LOGIN_FAILED =107;

    public static final int LOGIN_SUCCESS = 108;



    public static final int SEND_FEEDBACK =1001;

    public static final int SEND_FEEDBACK_SUCCESS =1002;

    public static final int SEND_FEEDBACK_FAILED =1003;


    public static final int GET_COMMUNICATION =1004;

    public static final int GET_COMMUNICATION_SUCCESS =1005;

    public static final int GET_COMMUNICATION_FAILED =1006;


    public static final int SEND_COMMUNICATION =1007;

    public static final int SEND_COMMUNICATION_SUCCESS =1008;

    public static final int SEND_COMMUNICATION_FAILED =1009;

    public static final int GET_EQUIPMENT_LIST =1010;

    public static final int GET_EQUIPMENT_LIST_SUCCESS =1011;

    public static final int GET_EQUIPMENT_LIST_FAILED =1012;

    public static final int UPDATE_CUSTOMER_STATUS_COMMUNICATION =1013;

    public static final int UPDATE_CUSTOMER_STATUS_COMMUNICATION_SUCCESS =1014;

    public static final int UPDATE_CUSTOMER_STATUS_COMMUNICATION_FAILED =1015;

    public static final int GET_ERROR_TYPE =1016;

    public static final int GET_ERROR_TYPE_SUCCESS =1017;

    public static final int GET_ERROR_TYPE_FAILED =1018;

    public static final int GET_COMPLAINTS_TYPE =1019;

    public static final int GET_ERROR_SUB_TYPE_SUCCESS =1020;

    public static final int GET_ERROR_SUB_TYPE_FAILED =1021;

    public static final int GET_CHECK_POINTS_TYPE =1022;

    public static final int GET_ERROR_CODE_TYPE_SUCCESS =1023;

    public static final int GET_ERROR_CODE_TYPE_FAILED =1024;

    public static final int GET_CHECK_PROCEDURE =1025;

    public static final int GET_ERROR_CODIFY_TYPE_SUCCESS =1026;

    public static final int GET_ERROR_CODIFY_TYPE_FAILED =1027;

    //SEND_WORK_STATUS

    public static final int SEND_WORK_STATUS =1028;

    public static final int SEND_WORK_STATUS_SUCCESS =1029;

    public static final int SEND_WORK_STATUS_FAILED =1030;

    public static final int SEND_SERVICE_REPORT =1031;

    public static final int SEND_SERVICE_REPORT_SUCCESS =1032;

    public static final int SEND_SERVICE_REPORT_FAILED =1033;

    public static final int NO_MORE_ERROR_CODE = 1034;

    public static final int SYNC_DATA_SUCCESS = 1035;

    public static final int SYNC_DATA_FAILED = 1036;

    public static final int GET_PRODUCT_GROUP = 1037;

    public static final int GET_COMPLAINTS_CONFIRMATION = 1038;

    public static final int SYNC_IMAGE = 1039;

    public static final int SYNC_IMAGE_SUCCESS = 1040;

    public static final int SYNC_IMAGE_FAILED = 1041;


   /* public static final int GET_NEWSLETTER =1004;

    public static final int GET_NEWSLETTER_SUCCESS =1005;

    public static final int GET_NEWSLETTER_FAILED =1006;*/


    public static String AUTHENTICATION_TOKEN ="",USER_ID="",EMP_NAME="",SHARED_PREF_NAME="service_eng",selectedRestaurantId ="",GOOGLE_REG_ID="";



}
