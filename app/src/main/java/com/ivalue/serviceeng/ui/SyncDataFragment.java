package com.ivalue.serviceeng.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ivalue.serviceeng.R;
import com.ivalue.serviceeng.bean.Constants;
import com.ivalue.serviceeng.dao.DAOFactory;
import com.ivalue.serviceeng.dao.CheckpointsDAO;
import com.ivalue.serviceeng.dao.ComplaintsDAO;
import com.ivalue.serviceeng.dao.ProductGroupDAO;
import com.ivalue.serviceeng.dto.CheckpointsDTO;
import com.ivalue.serviceeng.dto.ComplaintsDTO;
import com.ivalue.serviceeng.dto.ProductGroupDTO;
import com.ivalue.serviceeng.bean.ThreadDefine;
import com.ivalue.serviceeng.util.ConnectionDetector;
import com.ivalue.serviceeng.util.MaterialDialog;
import com.ivalue.serviceeng.util.ProgressBarCircular;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
/**
 * Created by Sasikumar on 6/16/2016.
 */
public class SyncDataFragment extends Fragment {

    private ProgressBarCircular progressBarCircular;

    private MaterialDialog matDialog;

    private ActionBar actionBar;

    private Activity thisActivity;

    private ReceiveCommTask receiveCommTask;



    private ArrayList<CheckpointsDTO> checkpointsDTOArrayList;

    private ArrayList<ComplaintsDTO> complaintsDTOArrayList;

    private ArrayList<ProductGroupDTO> productGroupDTOArrayList;

    private String[] pdfUrls,imageUrls;

    //private ArrayList<String> imageUrlArrList;
    private Resources resources;

    private SharedPreferences dashboardSharedPref;

    private ProgressDialog progressDialog;

    //private ProgressBar downloadProgressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View rootView = inflater.inflate(R.layout.sync_data_fragment, container,
                false);
        try {

            thisActivity = getActivity();

            matDialog = new MaterialDialog(thisActivity);

            progressBarCircular = (ProgressBarCircular) rootView.findViewById(R.id.product_progress);



            progressBarCircular.setVisibility(View.GONE);

            //downloadProgressBar  = (ProgressBar) rootView.findViewById(R.id.progress_bar_download);

            //downloadProgressBar.setVisibility(View.GONE);

            resources = thisActivity.getResources();

            dashboardSharedPref = thisActivity.getSharedPreferences(ThreadDefine.SHARED_PREF_NAME, Context.MODE_PRIVATE);


            if(ThreadDefine.AUTHENTICATION_TOKEN.length()==0)
                ThreadDefine.AUTHENTICATION_TOKEN  = dashboardSharedPref.getString(Constants.AUTH_TOKEN,"");


            if (new ConnectionDetector(thisActivity).isConnectingToInternet()) {

                newsLetterHandler.sendEmptyMessage(ThreadDefine.GET_COMMUNICATION);

            }else {

                matDialog = new MaterialDialog(thisActivity);

                matDialog.setMessage(resources.getString(R.string.no_internet)).setPositiveButton(resources.getString(R.string.ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        matDialog.dismiss();

                    }
                }).show();
            }

        } catch (Exception ex) {

            Log.d("", "" + ex.toString());

        }

        return rootView;

    }


    public Handler newsLetterHandler = new Handler(new Handler.Callback() {


        public boolean handleMessage(Message msg) {

            switch (msg.what) {

                case ThreadDefine.GET_COMMUNICATION:

                    receiveCommTask = new ReceiveCommTask();

                    receiveCommTask.execute(msg.what);

                    break;

                case ThreadDefine.GET_COMMUNICATION_SUCCESS:

                  try {

                      if (!checkpointsDTOArrayList.isEmpty()) {

                          CheckpointsDAO errorCodificationSqlDAO = (CheckpointsDAO) DAOFactory.createObjectDAO(thisActivity, CheckpointsDTO.class);
                          if(errorCodificationSqlDAO.getCount()>0)
                                 errorCodificationSqlDAO.deleteTableData();
                          int errCodiLength = checkpointsDTOArrayList.size();
                          pdfUrls = new String[errCodiLength];
                          imageUrls = new String[errCodiLength];
                          for (int erCo = 0; erCo < errCodiLength; erCo++) {
                              try {
                                  CheckpointsDTO checkpointsDTO = checkpointsDTOArrayList.get(erCo);
                                  errorCodificationSqlDAO.create(checkpointsDTO);
                                  String pdfUrl = checkpointsDTO.getFilePath();
                                  if (pdfUrl.length() > 0)
                                      pdfUrls[erCo] = pdfUrl;

                                  String imageFilePath = checkpointsDTO.getCheckImagePath();
                                  if(imageFilePath.length()>0)
                                      imageUrls[erCo] = imageFilePath;

                              } catch (Exception ex) {

                              }
                          }
                          checkpointsDTOArrayList = null;
                      }
                      if (!complaintsDTOArrayList.isEmpty()) {
                          ComplaintsDAO errorSubTypeSqlDAO = (ComplaintsDAO) DAOFactory.createObjectDAO(thisActivity, ComplaintsDTO.class);
                         if(errorSubTypeSqlDAO.getCount()>0)
                            errorSubTypeSqlDAO.deleteTableData();
                          int errorSubTypeLen = complaintsDTOArrayList.size();
                          for (int erCo = 0; erCo < errorSubTypeLen; erCo++) {
                              try {
                                  ComplaintsDTO complaintsDTO = complaintsDTOArrayList.get(erCo);
                                  errorSubTypeSqlDAO.create(complaintsDTO);
                              }catch(Exception ex){

                              }
                          }
                          complaintsDTOArrayList = null;
                      }
                      if (!productGroupDTOArrayList.isEmpty()) {
                          ProductGroupDAO errorTypeSqlDAO = (ProductGroupDAO) DAOFactory.createObjectDAO(thisActivity, ProductGroupDTO.class);
                          if(errorTypeSqlDAO.getCount()>0)
                                errorTypeSqlDAO.deleteTableData();
                          int errorTypeLen = productGroupDTOArrayList.size();
                          for (int erCo = 0; erCo < errorTypeLen; erCo++) {
                              try {
                                  ProductGroupDTO productGroupDTO = productGroupDTOArrayList.get(erCo);
                                  errorTypeSqlDAO.create(productGroupDTO);
                              } catch (Exception ex) {

                              }
                          }
                          productGroupDTOArrayList = null;
                      }
                      downloadImage(imageUrls);


                  }catch(Exception ex){
                      Log.d("Insert data Fail",""+ex.toString());
                      progressBarCircular.setVisibility(View.INVISIBLE);
                      matDialog = new MaterialDialog(thisActivity);
                      matDialog.setMessage(resources.getString(R.string.sync_failed)).setPositiveButton(resources.getString(R.string.ok), new View.OnClickListener() {
                          @Override
                          public void onClick(View v) {
                              matDialog.dismiss();
                          }
                      }).show();
                  }
                    break;

                case ThreadDefine.GET_COMMUNICATION_FAILED:

                    progressBarCircular.setVisibility(View.INVISIBLE);
                    matDialog = new MaterialDialog(thisActivity);
                    matDialog.setMessage(resources.getString(R.string.server_not_resp)).setPositiveButton(resources.getString(R.string.ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            matDialog.dismiss();

                        }
                    }).show();
                    break;
                case ThreadDefine.SYNC_DATA_SUCCESS:

                    progressBarCircular.setVisibility(View.INVISIBLE);
                    matDialog = new MaterialDialog(thisActivity);
                    matDialog.setMessage(resources.getString(R.string.sync_success)).setPositiveButton(resources.getString(R.string.ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            matDialog.dismiss();
                            ((DashboardActivity) thisActivity).selectPageFragment(ThreadDefine.GET_PRODUCT_GROUP, null);
                        }
                    }).show();
                    break;
                case ThreadDefine.SYNC_DATA_FAILED:
                    progressBarCircular.setVisibility(View.INVISIBLE);
                    matDialog = new MaterialDialog(thisActivity);
                    matDialog.setMessage(resources.getString(R.string.sync_failed)).setPositiveButton(resources.getString(R.string.ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            matDialog.dismiss();
                        }
                    }).show();

                  break;

                case ThreadDefine.SYNC_IMAGE:

                 break;

                case ThreadDefine.SYNC_IMAGE_SUCCESS:
                    downloadPDF(pdfUrls);
                break;

                case ThreadDefine.SYNC_IMAGE_FAILED:
                    downloadPDF(pdfUrls);
                break;
            }

            return  true;
        }
    });

    private void downloadImage(String[] pdfUrl){
        try {
            new ImageAsyncDownloader().execute(pdfUrl);
        }catch (Exception ex){

            Log.d("downloadImage ",""+ex.toString());
        }
    }

    private void downloadPDF(String[] pdfUrl){
        try {
            new AsyncDownloader().execute(pdfUrl);
        }catch (Exception ex){

            Log.d("downloadPDF ",""+ex.toString());
        }
    }
    private class ReceiveCommTask extends AsyncTask<Integer, Integer, Integer>

    {
        int result = 0;

        protected void onPreExecute() {

            super.onPreExecute();

            if (progressBarCircular != null) {

                progressBarCircular.setVisibility(View.VISIBLE);

                progressBarCircular.bringToFront();

            }

        }


        @Override
        protected Integer doInBackground(Integer... params) {


            switch (params[0]) {

                case ThreadDefine.GET_COMMUNICATION:

                    try {

                        OkHttpClient client = new OkHttpClient();

                        JSONObject productJson = new JSONObject();
                        MediaType JSON
                                = MediaType.parse("application/json; charset=utf-8");
                        productJson.put("auth_token",ThreadDefine.AUTHENTICATION_TOKEN);
                        RequestBody requestBody = RequestBody.create(JSON, productJson.toString()); // newsletter?user_id=10001

                       //http://cse.ivaluesoft.com/csewebserve/appservices.svc/serviceengg/error_codification_full/auth_token=e475e1e6-2428-4bbb-47c6-c99336a9

                        String syncUrl = resources.getString(R.string.app_url)+ "error_codification_full/auth_token="+ThreadDefine.AUTHENTICATION_TOKEN;
                        Log.d("Sync url ", "" + syncUrl);
                        Request request = new Request.Builder()
                                .url(syncUrl)
                                .post(requestBody)
                                .build();

                        Response response = client.newCall(request).execute();
                        JSONObject responseJsonObj = new JSONObject(response.body().string());
                        Log.d("Sync url ",""+responseJsonObj.toString());
                        if(responseJsonObj.has("status_code")) {

                            int statusCode = responseJsonObj.getInt("status_code");

                            if(statusCode==200){

                                if(responseJsonObj.has("checkpoints_full")){

                                    JSONArray newsLetterJSonObj = responseJsonObj.getJSONArray("checkpoints_full");

                                    Log.d("checkpoints_full ",""+newsLetterJSonObj.toString());

                                    int commLen = newsLetterJSonObj.length();
                                    checkpointsDTOArrayList = new ArrayList<CheckpointsDTO>();
                                    for(int comm=0;comm<commLen;comm++) {

                                        JSONObject newsLetterJSon =  newsLetterJSonObj.getJSONObject(comm);

                                        CheckpointsDTO newsLetterDTO = new CheckpointsDTO();

                                        if(!newsLetterJSon.isNull("checking_procedure"))
                                            newsLetterDTO.setCheckingProcedure(newsLetterJSon.getString("checking_procedure"));

                                        if(!newsLetterJSon.isNull("checkpoints_code"))
                                            newsLetterDTO.setCheckPointCode(newsLetterJSon.getString("checkpoints_code"));

                                        if(!newsLetterJSon.isNull("checkpoints_codeid"))
                                            newsLetterDTO.setCheckPointCodeId(newsLetterJSon.getInt("checkpoints_codeid"));

                                        if(!newsLetterJSon.isNull("checkpoints_header"))
                                            newsLetterDTO.setCheckPointProbableRootCause(newsLetterJSon.getString("checkpoints_header"));

                                        if(!newsLetterJSon.isNull("checkpoints_solution"))
                                            newsLetterDTO.setCheckPointSolution(newsLetterJSon.getString("checkpoints_solution"));

                                        if(!newsLetterJSon.isNull("complaints_typeid"))
                                            newsLetterDTO.setComplaintsTypeId(newsLetterJSon.getInt("complaints_typeid"));

                                        if(!newsLetterJSon.isNull("product_typeid"))
                                            newsLetterDTO.setProductTypeId(newsLetterJSon.getInt("product_typeid"));

                                        if(!newsLetterJSon.isNull("file_path")) {
                                            //String filePath = newsLetterJSon.getString("file_path");
                                            newsLetterDTO.setFilePath(newsLetterJSon.getString("file_path"));
                                          /* if(filePath.length()>0) {
                                               String filename = filePath.substring(filePath.lastIndexOf("/"));

                                           }else
                                               newsLetterDTO.setFilePath("");*/

                                        }

                                        if(!newsLetterJSon.isNull("check_points_image_url")) {
                                            newsLetterDTO.setCheckImagePath(newsLetterJSon.getString("check_points_image_url"));
                                        }

                                        checkpointsDTOArrayList.add(newsLetterDTO);
                                    }
                                }
                                if(responseJsonObj.has("complaints_full")) {

                                    JSONArray errorSubTypeJSonArr = responseJsonObj.getJSONArray("complaints_full");

                                    int errorSubTypeLen = errorSubTypeJSonArr.length();

                                    complaintsDTOArrayList = new ArrayList<ComplaintsDTO>(errorSubTypeLen);
                                    for(int errorSubType=0;errorSubType<errorSubTypeLen;errorSubType++) {

                                        JSONObject errorSubTypeJSonObj = errorSubTypeJSonArr.getJSONObject(errorSubType);

                                        ComplaintsDTO complaintsDTO = new ComplaintsDTO();

                                        if (!errorSubTypeJSonObj.isNull("complaints_type"))
                                            complaintsDTO.setComplaintsType(errorSubTypeJSonObj.getString("complaints_type"));

                                        if (!errorSubTypeJSonObj.isNull("conf_check_points"))
                                            complaintsDTO.setConfirmCheckPoint(errorSubTypeJSonObj.getString("conf_check_points"));

                                        if (!errorSubTypeJSonObj.isNull("definition"))
                                            complaintsDTO.setDefinition(errorSubTypeJSonObj.getString("definition"));

                                        if (!errorSubTypeJSonObj.isNull("product_typeid"))
                                            complaintsDTO.setProductId(errorSubTypeJSonObj.getInt("product_typeid"));

                                        if (!errorSubTypeJSonObj.isNull("complaints_typeid"))
                                            complaintsDTO.setComplaintsTypeId(errorSubTypeJSonObj.getInt("complaints_typeid"));

                                        complaintsDTOArrayList.add(complaintsDTO);
                                    }
                                }
                                if(responseJsonObj.has("product_group_full")) {

                                    JSONArray errorSubTypeJSonArr = responseJsonObj.getJSONArray("product_group_full");

                                    int errorSubTypeLen = errorSubTypeJSonArr.length();

                                    productGroupDTOArrayList = new ArrayList<ProductGroupDTO>(errorSubTypeLen);

                                    for(int errorSubType=0;errorSubType<errorSubTypeLen;errorSubType++) {

                                        JSONObject errorSubTypeJSonObj = errorSubTypeJSonArr.getJSONObject(errorSubType);

                                        ProductGroupDTO productGroupDTO = new ProductGroupDTO();

                                        if (!errorSubTypeJSonObj.isNull("product_type"))
                                            productGroupDTO.setProductType(errorSubTypeJSonObj.getString("product_type"));

                                        if (!errorSubTypeJSonObj.isNull("product_typeid"))
                                            productGroupDTO.setProductId(errorSubTypeJSonObj.getInt("product_typeid"));
                                        productGroupDTOArrayList.add(productGroupDTO);
                                    }
                                }
                                result = ThreadDefine.GET_COMMUNICATION_SUCCESS;


                            }else
                                result = ThreadDefine.GET_COMMUNICATION_FAILED;

                        }else
                            result = ThreadDefine.GET_COMMUNICATION_FAILED;


                    }catch (Exception ex){

                        result = ThreadDefine.GET_COMMUNICATION_FAILED;

                        Log.d("","Task Exception "+ex.toString());
                    }

                    break;

            }

            return result;
        }

        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);

          /*  if (progressBarCircular != null)
                progressBarCircular.setVisibility(View.GONE);*/

            newsLetterHandler.sendEmptyMessage(result);
        }
    }

    private class AsyncDownloader extends AsyncTask<String, Integer, String> {

        //private String URL = "";
        private String filePath ="";
        private File destFile;
        private int downloaded = 0;
        private int urlLen = 0 ;
        public AsyncDownloader(){
           //URL = url;


        }
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressBarCircular != null) {
                progressBarCircular.setVisibility(View.VISIBLE);
                progressBarCircular.bringToFront();
            }
            progressDialog = new ProgressDialog(thisActivity);
            progressDialog.setTitle("Downloading Documents");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setIcon(R.drawable.ico_download);
            progressDialog.setCancelable(true);
            progressDialog.show();

        }
        @Override
        protected String doInBackground(String... urls) {
            filePath = Environment.getExternalStorageDirectory().toString() + getString(R.string.folder_downloads);
            File folder = new File(filePath);
            Log.d(pdfUrls.length+" Folder " + filePath, "" + folder.exists());
            if (!folder.exists())
                folder.mkdirs();

            int isInterrupt = 0;
            for(String url : pdfUrls) {
                Log.d("Pdf Url : "+url,""+urlLen);
                urlLen++;
                if(url!=null) {
                    if (downloadPdfFile(url, urlLen) == 1)
                        isInterrupt = 1;
                }
            }
            if(isInterrupt==0)
                return "true";
            else
                return "false";
        }

        private int downloadPdfFile(String url,int urlLen){
            Log.d("No :" + urlLen + ": Url " + url, "");
            int isInterrupt = 0;

                    if (url.length() > 0) {
                        OutputStream out = null;
                        OkHttpClient httpClient = new OkHttpClient();
                        Call call = httpClient.newCall(new Request.Builder().url(url).get().build());
                        try {
                            String filename = url.substring(url.lastIndexOf("/"));
                            destFile = new File(filePath + filename);
                            if (destFile.exists()) {
                                if (destFile.canRead() && destFile.canWrite()) {
                                    downloaded = (int) destFile.length();
                                } else {
                                    destFile.delete();
                                    downloaded = 0;
                                }
                            }
                            out = downloaded == 0 ? new FileOutputStream(destFile) : new FileOutputStream(destFile, true);

                            Response response = call.execute();
                            if (response.code() == 200) {
                                InputStream inputStream = null;
                                try {
                                    inputStream = response.body().byteStream();
                                    byte[] buff = new byte[1024 * 4];
                                    long downloaded = 0;
                                    long target = response.body().contentLength();
                                    int length = 0;
                                    while ((length = inputStream.read(buff)) > 0) {
                                        downloaded += length;
                                        publishProgress((int) ((downloaded * 100) / target),urlLen);
                                        out.write(buff, 0, length);
                                    }
                                } catch (IOException ignore) {
                                    // return "false";
                                    isInterrupt = 1;
                                } finally {
                                    if (inputStream != null) {
                                        inputStream.close();
                                    }
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            isInterrupt = 1;
                        }
                    }
            return isInterrupt;
        }
        protected void onProgressUpdate(Integer... values) {
           /* downloadProgressBar.setMax(values[1].intValue());
            downloadProgressBar.setProgress(values[0].intValue());*/
            progressDialog.setProgressNumberFormat(null);
            progressDialog.setProgress(values[0]);
            progressDialog.setMessage("Downloading " +(values[1]) + "/" + (pdfUrls.length));
        }

        @Override
        protected void onPostExecute(String result) {
            // textViewStatus.setText(result ? "Downloaded" : "Failed");
            //downloadProgressBar.setVisibility(View.GONE);
            progressDialog.dismiss();
           if(result.equals("true"))
               newsLetterHandler.sendEmptyMessage(ThreadDefine.SYNC_DATA_SUCCESS);
           else
               newsLetterHandler.sendEmptyMessage(ThreadDefine.SYNC_DATA_FAILED);


        }
    }


    private class ImageAsyncDownloader extends AsyncTask<String, Integer, String> {

        //private String URL = "";
        private String filePath ="";
        private File destFile;
        private int downloaded = 0;
        private int urlLen = 0 ;
        public ImageAsyncDownloader(){
            //URL = url;


        }
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressBarCircular != null) {
                progressBarCircular.setVisibility(View.VISIBLE);
                progressBarCircular.bringToFront();
            }
            progressDialog = new ProgressDialog(thisActivity);
            progressDialog.setTitle("Downloading Image");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setIcon(R.drawable.ico_download);
            progressDialog.setCancelable(true);
            progressDialog.show();

        }
        @Override
        protected String doInBackground(String... urls) {
            filePath = Environment.getExternalStorageDirectory().toString() + getString(R.string.folder_downloads);
            File folder = new File(filePath);
            Log.d(imageUrls.length+" Folder " + filePath, "" + folder.exists());
            if (!folder.exists())
                folder.mkdirs();

            int isInterrupt = 0;
            for(String url : imageUrls) {
                Log.d("Image Url : "+url,""+urlLen);
                urlLen++;
                if(url!=null) {
                    if (downloadImageFile(url, urlLen) == 1)
                        isInterrupt = 1;
                }
            }
            if(isInterrupt==0)
                return "true";
            else
                return "false";
        }

        private int downloadImageFile(String url,int urlLen){
            Log.d("No :"+urlLen+": Url "+url,"");
            int isInterrupt = 0;

            if (url.length() > 0) {
                OutputStream out = null;
                OkHttpClient httpClient = new OkHttpClient();
                Call call = httpClient.newCall(new Request.Builder().url(url).get().build());
                try {
                    String filename = url.substring(url.lastIndexOf("/"));
                    destFile = new File(filePath + filename);
                    if (destFile.exists()) {
                        if (destFile.canRead() && destFile.canWrite()) {
                            downloaded = (int) destFile.length();
                        } else {
                            destFile.delete();
                            downloaded = 0;
                        }
                    }
                    out = downloaded == 0 ? new FileOutputStream(destFile) : new FileOutputStream(destFile, true);

                    Response response = call.execute();
                    if (response.code() == 200) {
                        InputStream inputStream = null;
                        try {
                            inputStream = response.body().byteStream();
                            byte[] buff = new byte[1024 * 4];
                            long downloaded = 0;
                            long target = response.body().contentLength();
                            int length = 0;
                            while ((length = inputStream.read(buff)) > 0) {
                                downloaded += length;
                                publishProgress((int) ((downloaded * 100) / target),urlLen);
                                out.write(buff, 0, length);
                            }
                        } catch (IOException ignore) {
                            // return "false";
                            isInterrupt = 1;
                        } finally {
                            if (inputStream != null) {
                                inputStream.close();
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    isInterrupt = 1;
                }
            }
            return isInterrupt;
        }
        protected void onProgressUpdate(Integer... values) {
           /* downloadProgressBar.setMax(values[1].intValue());
            downloadProgressBar.setProgress(values[0].intValue());*/
            progressDialog.setProgressNumberFormat(null);
            progressDialog.setProgress(values[0]);
            progressDialog.setMessage("Downloading Image" +(values[1]) + "/" + (imageUrls.length));
        }

        @Override
        protected void onPostExecute(String result) {
            // textViewStatus.setText(result ? "Downloaded" : "Failed");
            //downloadProgressBar.setVisibility(View.GONE);
            progressDialog.dismiss();
            if(result.equals("true"))
                newsLetterHandler.sendEmptyMessage(ThreadDefine.SYNC_IMAGE_SUCCESS);
            else
                newsLetterHandler.sendEmptyMessage(ThreadDefine.SYNC_IMAGE_FAILED);


        }
    }
}

