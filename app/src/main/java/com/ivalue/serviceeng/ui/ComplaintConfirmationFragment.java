package com.ivalue.serviceeng.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ivalue.serviceeng.R;
import com.ivalue.serviceeng.adapter.ComplaintsAdapter;
import com.ivalue.serviceeng.bean.Constants;
import com.ivalue.serviceeng.bean.ThreadDefine;
import com.ivalue.serviceeng.dao.ComplaintsDAO;
import com.ivalue.serviceeng.dao.DAOFactory;
import com.ivalue.serviceeng.dto.ComplaintsDTO;
import com.ivalue.serviceeng.util.MaterialDialog;
import com.ivalue.serviceeng.util.ProgressBarCircular;

import java.util.ArrayList;

/**
 * Created by Sasikumar on 7/13/2016.
 */
public class ComplaintConfirmationFragment  extends Fragment {

    //private ProgressBarCircular progressBarCircular;

    private MaterialDialog matDialog;

    private ActionBar actionBar;

    private Activity thisActivity;

    //private ErrorSubTypeTask errorSubTypeTask;

    //private ComplaintsAdapter complaintsAdapter;

    //private ArrayList<ComplaintsDTO> errorSubTypeDTOArrayList;

    private Resources resources;

    private TextView noMoreNews;

    //private ListView newsLetterListView;

    private SharedPreferences dashboardSharedPref;

    private int productId = 0,complaintsId=0;

    private Bundle bundle;

    private String productType= "",complaintsStr="",definitionStr="",confirmationStr="";

    private TextView headerTxt,complaintsTxt,definitionTxt,confirmationTxt;

    private ImageView proceedImgview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View rootView = inflater.inflate(R.layout.complaint_confirmation, container,
                false);
        try {

            thisActivity = getActivity();
            matDialog = new MaterialDialog(thisActivity);
            bundle = getArguments();

            if(bundle.containsKey("product_id"))
                productId = bundle.getInt("product_id");

            if(bundle.containsKey("product_type"))
                productType = bundle.getString("product_type");

            if(bundle.containsKey("complaints_id"))
                complaintsId = bundle.getInt("complaints_id");

            if(bundle.containsKey("complaints_type"))
                complaintsStr= bundle.getString("complaints_type");

            if(bundle.containsKey("definition"))
                 definitionStr= bundle.getString("definition");

            if(bundle.containsKey("confirm_check_point"))
                     confirmationStr= bundle.getString("confirm_check_point");

            //progressBarCircular = (ProgressBarCircular) rootView.findViewById(R.id.product_progress);

            //progressBarCircular.setVisibility(View.GONE);

            resources = thisActivity.getResources();
            dashboardSharedPref = thisActivity.getSharedPreferences(ThreadDefine.SHARED_PREF_NAME, Context.MODE_PRIVATE);

            headerTxt = (TextView) rootView.findViewById(R.id.title_txt);

            complaintsTxt  = (TextView) rootView.findViewById(R.id.complaints_txt);

            definitionTxt  = (TextView) rootView.findViewById(R.id.definition_txt);

            confirmationTxt  = (TextView) rootView.findViewById(R.id.confirm_check_txt);

            headerTxt.setText(resources.getString(R.string.compliants_confirmation));

            if(ThreadDefine.AUTHENTICATION_TOKEN.length()==0)
                ThreadDefine.AUTHENTICATION_TOKEN  = dashboardSharedPref.getString(Constants.AUTH_TOKEN,"");

            proceedImgview = (ImageView) rootView.findViewById(R.id.proceed_img);

            proceedImgview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Bundle errorBundle = new Bundle();
                    errorBundle.putInt("product_id", productId);
                    errorBundle.putInt("complaints_id", complaintsId);
                    errorBundle.putString("complaints_type", complaintsStr);

                    ((DashboardActivity) thisActivity).selectPageFragment(ThreadDefine.GET_CHECK_POINTS_TYPE, errorBundle);
                }
            });

            complaintsTxt.setText(complaintsStr);

            definitionTxt.setText(definitionStr);

            confirmationTxt.setText(confirmationStr);

        } catch (Exception ex) {
            Log.d("", "" + ex.toString());
        }
        return rootView;

    }


    public Handler newsLetterHandler = new Handler(new Handler.Callback() {


        public boolean handleMessage(Message msg) {

            switch (msg.what) {

               /* case ThreadDefine.GET_ERROR_SUB_TYPE:

                    errorSubTypeTask = new ErrorSubTypeTask();

                    errorSubTypeTask.execute(msg.what);

                    break;*/

                case ThreadDefine.GET_ERROR_SUB_TYPE_SUCCESS:

                   /* if(errorSubTypeDTOArrayList.size()>0) {

                        complaintsAdapter = new ComplaintsAdapter(thisActivity, errorSubTypeDTOArrayList);

                        newsLetterListView.setAdapter(complaintsAdapter);

                        complaintsAdapter.notifyDataSetChanged();

                        noMoreNews.setVisibility(View.GONE);

                        newsLetterListView.setVisibility(View.VISIBLE);

                    }else{

                        noMoreNews.setVisibility(View.VISIBLE);

                        newsLetterListView.setVisibility(View.GONE);
                    }*/


                    break;
                case ThreadDefine.NO_MORE_ERROR_CODE:
                    noMoreNews.setVisibility(View.VISIBLE);
                    noMoreNews.setText("No more records to show");
                    // newsLetterListView.setVisibility(View.GONE);

                    break;

                case ThreadDefine.GET_ERROR_SUB_TYPE_FAILED:

                    //progressBarCircular.setVisibility(View.INVISIBLE);

                    matDialog = new MaterialDialog(thisActivity);

                    matDialog.setMessage(resources.getString(R.string.server_not_resp)).setPositiveButton(resources.getString(R.string.ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            matDialog.dismiss();

                        }
                    }).show();

                    break;
            }

            return  true;
        }
    });

}
