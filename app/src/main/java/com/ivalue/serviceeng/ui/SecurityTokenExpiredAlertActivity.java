package com.ivalue.serviceeng.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;

import com.ivalue.serviceeng.R;
import com.ivalue.serviceeng.bean.Constants;
import com.ivalue.serviceeng.bean.ThreadDefine;

import java.io.File;

/**
 * Created by Sasikumar on 9/16/2016.
 */
public class SecurityTokenExpiredAlertActivity  extends Activity{

    public static final String EXTRA_EXIT_TO_LOGIN = "EXTRA_EXIT";

    private SharedPreferences securityTokenPref;

    private Context securityContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.security_token_expire_activity);

        securityTokenPref = getSharedPreferences(ThreadDefine.SHARED_PREF_NAME, Context.MODE_PRIVATE);

        securityContext = getApplicationContext();

        createExpriredDialog().show();
    }


    private void storeSharedPref() {
        ThreadDefine.AUTHENTICATION_TOKEN = "";
        if(securityTokenPref!=null) {

            SharedPreferences.Editor loginEditor = securityTokenPref.edit();

            loginEditor.putString(Constants.AUTH_TOKEN, "");

            loginEditor.putString(Constants.USER_ID, "");

            loginEditor.putLong(Constants.SECURITY_TOKEN_EXPIRE_TIME,0L);

            loginEditor.commit();

        }

    }
    private Dialog createExpriredDialog() {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(SecurityTokenExpiredAlertActivity.this);
        builder.setTitle("Login Expired")
                .setMessage("Please login again")
                .setCancelable(false)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                                securityContext.deleteDatabase(securityContext.getString(R.string.db_name));
                                String filePath = Environment.getExternalStorageDirectory().toString() + getString(R.string.folder_downloads);
                                File folder = new File(filePath);
                                deleteDirectory(folder);
                                storeSharedPref();
                                Intent intent =
                                        new Intent(SecurityTokenExpiredAlertActivity.this,LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                        | Intent.FLAG_ACTIVITY_SINGLE_TOP
                                        | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                                intent.putExtra(EXTRA_EXIT_TO_LOGIN, true);
                                startActivity(intent);
                            }
                        });
        return builder.create();
    }

   private boolean deleteDirectory(File path) {
        if( path.exists() ) {
            File[] files = path.listFiles();
            if (files == null) {
                return true;
            }
            for(int i=0; i<files.length; i++) {
                if(files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                }
                else {
                    files[i].delete();
                }
            }
        }
        return( path.delete() );
    }

}