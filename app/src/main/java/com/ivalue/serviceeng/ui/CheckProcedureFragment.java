package com.ivalue.serviceeng.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.ivalue.serviceeng.R;
import com.ivalue.serviceeng.adapter.CheckingProcedureAdapter;

import com.ivalue.serviceeng.bean.Constants;
import com.ivalue.serviceeng.bean.ThreadDefine;
import com.ivalue.serviceeng.dao.DAOFactory;
import com.ivalue.serviceeng.dao.CheckpointsDAO;
import com.ivalue.serviceeng.dto.CheckpointsDTO;

import com.ivalue.serviceeng.util.MaterialDialog;
import com.ivalue.serviceeng.util.ProgressBarCircular;

import java.util.ArrayList;

/**
 * Created by Sasikumar on 5/17/2016.
 */
public class CheckProcedureFragment extends Fragment {

    private ProgressBarCircular progressBarCircular;

    private MaterialDialog matDialog;

    private ActionBar actionBar;

    private Activity thisActivity;

    private CheckingProcedureAdapter checkingProcedureAdapter;

    private ArrayList<CheckpointsDTO> checkpointsDTOArrayList;

    private Resources resources;

    private TextView noMoreNews;

    private ListView errorCodificationListView;

    private SharedPreferences dashboardSharedPref;
    private Bundle bundle;
    private int checkPointId = 0;

    private TextView headerTxt;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View rootView = inflater.inflate(R.layout.receive_communication_fragment, container,
                false);
        try {

            thisActivity = getActivity();

            matDialog = new MaterialDialog(thisActivity);
            progressBarCircular = (ProgressBarCircular) rootView.findViewById(R.id.product_progress);
            progressBarCircular.setVisibility(View.GONE);
            resources = thisActivity.getResources();

            headerTxt = (TextView) rootView.findViewById(R.id.title_txt);

            bundle = getArguments();
            if (bundle.containsKey("checkpoint_code"))
                checkPointId = bundle.getInt("checkpoint_code");

            headerTxt.setText(resources.getString(R.string.checking_procedure));

            dashboardSharedPref = thisActivity.getSharedPreferences(ThreadDefine.SHARED_PREF_NAME, Context.MODE_PRIVATE);

            if (ThreadDefine.AUTHENTICATION_TOKEN.length() == 0)
                ThreadDefine.AUTHENTICATION_TOKEN = dashboardSharedPref.getString(Constants.AUTH_TOKEN, "");

            noMoreNews = (TextView) rootView.findViewById(R.id.no_news_letter_txt);

            errorCodificationListView = (ListView) rootView.findViewById(R.id.news_letter_list);

            checkpointsDTOArrayList = new ArrayList<CheckpointsDTO>();

            CheckpointsDAO errorCodificationSqlDAO = (CheckpointsDAO) DAOFactory.createObjectDAO(thisActivity, CheckpointsDTO.class);

            checkpointsDTOArrayList = errorCodificationSqlDAO.findGroupsByCheckPointId(checkPointId);

            if (checkpointsDTOArrayList.size() > 0) {

                checkingProcedureAdapter = new CheckingProcedureAdapter(thisActivity, checkpointsDTOArrayList);
                errorCodificationListView.setAdapter(checkingProcedureAdapter);
                checkingProcedureAdapter.notifyDataSetChanged();
                noMoreNews.setVisibility(View.GONE);
                errorCodificationListView.setVisibility(View.VISIBLE);

            } else {
                headerTxt.setVisibility(View.GONE);
                noMoreNews.setText(resources.getString(R.string.no_data_show));
                noMoreNews.setVisibility(View.VISIBLE);
                errorCodificationListView.setVisibility(View.GONE);
            }

            /*if (new ConnectionDetector(thisActivity).isConnectingToInternet()) {

                newsLetterHandler.sendEmptyMessage(ThreadDefine.GET_ERROR_CODIFY_TYPE);

            }else {

                matDialog = new MaterialDialog(thisActivity);

                matDialog.setMessage(resources.getString(R.string.no_internet)).setPositiveButton(resources.getString(R.string.ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        matDialog.dismiss();

                    }
                }).show();
            }*/

        } catch (Exception ex) {

            Log.d("", "" + ex.toString());

        }

        return rootView;

    }


    public Handler newsLetterHandler = new Handler(new Handler.Callback() {


        public boolean handleMessage(Message msg) {

            switch (msg.what) {

                /*case ThreadDefine.GET_ERROR_CODIFY_TYPE:

                    receiveCommTask = new ReceiveCommTask();

                    receiveCommTask.execute(msg.what);

                    break;*/

                case ThreadDefine.GET_ERROR_CODIFY_TYPE_SUCCESS:

                    if (checkpointsDTOArrayList.size() > 0) {

                        checkingProcedureAdapter = new CheckingProcedureAdapter(thisActivity, checkpointsDTOArrayList);
                        errorCodificationListView.setAdapter(checkingProcedureAdapter);
                        checkingProcedureAdapter.notifyDataSetChanged();
                        noMoreNews.setVisibility(View.GONE);
                        errorCodificationListView.setVisibility(View.VISIBLE);

                    } else {

                        noMoreNews.setVisibility(View.VISIBLE);
                        errorCodificationListView.setVisibility(View.GONE);
                    }


                    break;
                case ThreadDefine.NO_MORE_ERROR_CODE:
                    noMoreNews.setVisibility(View.VISIBLE);
                    noMoreNews.setText("No more records to show");
                    errorCodificationListView.setVisibility(View.GONE);

                    break;

                case ThreadDefine.GET_ERROR_CODIFY_TYPE_FAILED:

                    progressBarCircular.setVisibility(View.INVISIBLE);
                    matDialog = new MaterialDialog(thisActivity);
                    matDialog.setMessage(resources.getString(R.string.server_not_resp)).setPositiveButton(resources.getString(R.string.ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            matDialog.dismiss();

                        }
                    }).show();

                    break;
            }
            return true;
        }
    });

    /*private class ReceiveCommTask extends AsyncTask<Integer, Integer, Integer>

    {
        int result = 0;

        protected void onPreExecute() {

            super.onPreExecute();

            if (progressBarCircular != null) {

                progressBarCircular.setVisibility(View.VISIBLE);

                progressBarCircular.bringToFront();
            }
        }

        @Override
        protected Integer doInBackground(Integer... params) {

            switch (params[0]) {

                case ThreadDefine.GET_ERROR_CODIFY_TYPE:

                    try {

                        OkHttpClient client = new OkHttpClient();
                        JSONObject productJson = new JSONObject();

                        MediaType JSON
                                = MediaType.parse("application/json; charset=utf-8");
                        RequestBody formBody = new FormEncodingBuilder()
                                .add("err_codeid", String.valueOf(errorCodeId))
                                .build();
                        Request request = new Request.Builder()
                                .url(resources.getString(R.string.app_url)+ "error_codification/auth_token="+ThreadDefine.AUTHENTICATION_TOKEN+"&err_codeid="+errorCodeId)
                                .post(formBody)
                                .build();
                        Response response = client.newCall(request).execute();
                        JSONObject responseJsonObj = new JSONObject(response.body().string());
                        if(responseJsonObj.has("status_code")) {

                            int statusCode = responseJsonObj.getInt("status_code");
                            if(statusCode==200){

                                if(responseJsonObj.has("error_codification")){
                                    JSONArray newsLetterJSonObj = responseJsonObj.getJSONArray("error_codification");
                                    int commLen = newsLetterJSonObj.length();
                                    checkpointsDTOArrayList = new ArrayList<CheckpointsDTO>();
                                    for(int comm=0;comm<commLen;comm++) {

                                        JSONObject errorCodificationJSon =  newsLetterJSonObj.getJSONObject(comm);
                                        CheckpointsDTO checkpointsDTO = new CheckpointsDTO();

                                        if(!errorCodificationJSon.isNull("error_code"))
                                            checkpointsDTO.setErrorCode(errorCodificationJSon.getString("error_code"));

                                        if(!errorCodificationJSon.isNull("error_codeid"))
                                            checkpointsDTO.setErrorCodeId(errorCodificationJSon.getInt("error_codeid"));

                                        if(!errorCodificationJSon.isNull("error_solution"))
                                            checkpointsDTO.setErrorSolution(errorCodificationJSon.getString("error_solution"));

                                        checkpointsDTOArrayList.add(checkpointsDTO);
                                    }

                                    result = ThreadDefine.GET_ERROR_CODIFY_TYPE_SUCCESS;
                                }else
                                    result = ThreadDefine.GET_ERROR_CODIFY_TYPE_FAILED;


                            }else if(statusCode == 300)
                                  result = ThreadDefine.NO_MORE_ERROR_CODE;
                            else
                                result = ThreadDefine.GET_ERROR_CODIFY_TYPE_FAILED;

                        }else
                            result = ThreadDefine.GET_ERROR_CODIFY_TYPE_FAILED;




                    }catch (Exception ex){

                        result = ThreadDefine.GET_ERROR_CODIFY_TYPE_FAILED;

                        Log.d("","Receive Comm Task Exception "+ex.toString());
                    }

                    break;
            }

            return result;
        }

        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);

            if (progressBarCircular != null)
                progressBarCircular.setVisibility(View.GONE);

            newsLetterHandler.sendEmptyMessage(result);
        }
    }*/
}