package com.ivalue.serviceeng.ui;



import android.app.Activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.res.Resources;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.ivalue.serviceeng.R;
import com.ivalue.serviceeng.bean.Constants;
import com.ivalue.serviceeng.bean.ThreadDefine;
import com.ivalue.serviceeng.location.SecurityTokenExpiredListener;
import com.ivalue.serviceeng.util.ConnectionDetector;
import com.ivalue.serviceeng.util.MaterialDialog;
import com.ivalue.serviceeng.util.ProgressBarCircular;
import com.ivalue.serviceeng.util.ServiceEngUtil;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class LoginActivity extends Activity implements View.OnClickListener

{
    private EditText edtUserName, edtPassword;

    private Button btnLogin;

    private ProgressBarCircular progressBarCircular;

    private MaterialDialog matDialog;

    private String usernameStr="",passwordStr="";

    private Resources resources;

    //private List<RestaurantsDTO> restaurantList;

    private Set<String> restaurantIdSet,restaurantNameSet;

    private LoginTask loginTask;

    private SharedPreferences loginSharedPref;

    StringBuilder dayTypeStringBuilder;
    //= new StringBuilder();
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private GoogleCloudMessaging googleCloudMessaging;

    private ProgressDialog progressDialog;

    private int versionCheck = 0;

    private String apkDownloadUrl = "";

    private  TextView versionTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_login);

        loginSharedPref = getSharedPreferences(ThreadDefine.SHARED_PREF_NAME, Context.MODE_PRIVATE);

        matDialog = new MaterialDialog(this);

        progressBarCircular = (ProgressBarCircular) findViewById(R.id.login_progress);

        progressBarCircular.setVisibility(View.GONE);

        versionTxt = (TextView) findViewById(R.id.version_txt);

        edtUserName = (EditText) findViewById(R.id.edt_username);

        edtUserName.setText("ganesh@itc.com");

        edtPassword = (EditText) findViewById(R.id.edt_password);

        edtPassword.setText("ces_ganesh*");

        btnLogin = (Button) findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(this);

        resources = getResources();

        versionTxt.setText(" Ver : "+ServiceEngUtil.getCurrentVersion(this).versionName);
    }


    private void storeSharedPref() {

        if(loginSharedPref!=null) {

            SharedPreferences.Editor loginEditor = loginSharedPref.edit();

            loginEditor.putString(Constants.AUTH_TOKEN,ThreadDefine.AUTHENTICATION_TOKEN);

            loginEditor.putString(Constants.USER_ID, ThreadDefine.USER_ID);

            loginEditor.putString(Constants.GOOGLE_ID, ThreadDefine.GOOGLE_REG_ID);

            long expireDatELong = toMilliSeconds(30);

            loginEditor.putLong(Constants.SECURITY_TOKEN_EXPIRE_TIME,expireDatELong);
            //loginEditor.putString(Constants.EMP_NAME,ThreadDefine.EMP_NAME);

            loginEditor.commit();

            setSecurityTokenExpire();

        }
    }
    private void setSecurityTokenExpire() {

        long securityTokenExpireTime =
                 loginSharedPref.getLong(Constants.SECURITY_TOKEN_EXPIRE_TIME,0L);

        if(securityTokenExpireTime>0L) {
            Intent intent = new Intent(this, SecurityTokenExpiredListener.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    this.getApplicationContext(), 234324243, intent, 0);
            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, securityTokenExpireTime, pendingIntent);
        }
       /* DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        long milliSeconds=  securityTokenExpireTime;
        System.out.println(milliSeconds);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);*/
       // Toast.makeText(this, "Alarm set in "+formatter.format(calendar.getTime()) +" seconds",Toast.LENGTH_LONG).show();
    }
    private long toMilliSeconds(double day)
    {
        return (long) (System.currentTimeMillis()+ (long) (day * 24 * 60 * 60 * 1000));
    }

    private Handler loginHandler = new Handler(new Handler.Callback() {


        public boolean handleMessage(Message msg) {

            switch (msg.what) {

                case ThreadDefine.LOGIN:


                    loginTask = new LoginTask();

                    loginTask.execute(msg.what);

                break;

                case ThreadDefine.LOGIN_FAILED:

                  if(checkHigherVerison()) {


                  } else {

                      storeSharedPref();

                      String loginRespMsg = resources.getString(R.string.login_fail);

                      matDialog = new MaterialDialog(LoginActivity.this);

                      matDialog.setMessage(loginRespMsg).setPositiveButton(resources.getString(R.string.ok), new View.OnClickListener() {
                          @Override
                          public void onClick(View v) {

                              matDialog.dismiss();

                          }
                      }).show();
                  }

                break;

                case ThreadDefine.LOGIN_SUCCESS:



                    storeSharedPref();

                    String loginSuccesMsg = resources.getString(R.string.welcome);

                    matDialog = new MaterialDialog(LoginActivity.this);

                    matDialog.setMessage(loginSuccesMsg).setPositiveButton(resources.getString(R.string.ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            matDialog.dismiss();

                           Intent loginInt = new Intent(LoginActivity.this,DashboardActivity.class);

                            startActivity(loginInt);

                            LoginActivity.this.finish();


                       }
                    }).show();

                    break;

            }

            return  true;
        }

    });

    private class  LoginTask extends AsyncTask<Integer, Integer, Integer> {

        int result = 0;

        protected void onPreExecute() {

            super.onPreExecute();

            if (progressBarCircular != null) {

                progressBarCircular.setVisibility(View.VISIBLE);

                progressBarCircular.bringToFront();

            }
        }

        @Override
        protected Integer doInBackground(Integer... params) {
            switch (params[0]) {


                case ThreadDefine.LOGIN:

                  try {

                      OkHttpClient client = new OkHttpClient();

                      /*JSONObject loginJson = new JSONObject();

                      loginJson.put("username", Base64.encodeToString(usernameStr.getBytes("UTF-8"), Base64.DEFAULT));

                      loginJson.put("password", Base64.encodeToString(passwordStr.getBytes("UTF-8"), Base64.DEFAULT));

                      MediaType JSON
                              = MediaType.parse("application/json; charset=utf-8");

                      RequestBody requestBody = RequestBody.create(JSON, loginJson.toString());*/


                      String loginUrl = resources.getString(R.string.app_url)+"authenticate/email="+usernameStr+"&password="+passwordStr
                              +"&category_id="+resources.getString(R.string.app_category);

                      Request request = new Request.Builder().url(loginUrl).build();

                      Response response = client.newCall(request).execute();

                      String loginrespStr =response.body().string();

                      JSONObject loginRespJson = new JSONObject(loginrespStr);

                      Log.d("", "Login response --->"+loginRespJson.toString());

                      Log.d("","Url --->"+loginUrl);


                      if(loginRespJson.has("Token")) {
                          if(!loginRespJson.isNull("Token")) {

                              ThreadDefine.AUTHENTICATION_TOKEN = loginRespJson.getString("Token");

                              if(ThreadDefine.AUTHENTICATION_TOKEN.equalsIgnoreCase("Fail") || ThreadDefine.AUTHENTICATION_TOKEN.equalsIgnoreCase("Error")) {
                                  result = ThreadDefine.LOGIN_FAILED;
                                  ThreadDefine.AUTHENTICATION_TOKEN = "";
                              }else
                                  result = ThreadDefine.LOGIN_SUCCESS;

                          }else {

                              result = ThreadDefine.LOGIN_FAILED;
                          }
                      }
                      else
                          result = ThreadDefine.LOGIN_FAILED;

                  }catch(Exception ex){

                      Log.d("Login Exception ",""+ex.toString());

                      result = ThreadDefine.LOGIN_FAILED;
                  }


                    break;

            }
            return result;
        }

        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);


            if (progressBarCircular != null)
                progressBarCircular.setVisibility(View.GONE);

            loginHandler.sendEmptyMessage(result);
        }


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_login:

                View requestFocus;

                usernameStr = edtUserName.getText().toString().trim();

                passwordStr = edtPassword.getText().toString().trim();

                edtUserName.setError(null);

                edtPassword.setError(null);

                if(TextUtils.isEmpty(usernameStr)){

                    requestFocus = edtUserName;

                    requestFocus.requestFocus();

                    edtUserName.setError(resources.getString(R.string.enter_username));

                    /*matDialog.setMessage(resources.getString(R.string.enter_username)).setPositiveButton(resources.getString(R.string.ok), new OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            matDialog.dismiss();
                        }
                    });*/

                }else if(TextUtils.isEmpty(passwordStr)){

                    requestFocus = edtPassword;

                    requestFocus.requestFocus();

                    edtPassword.setError(resources.getString(R.string.enter_password));

                    /*matDialog.setMessage(resources.getString(R.string.enter_password)).setPositiveButton(resources.getString(R.string.ok), new OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            matDialog.dismiss();
                        }
                    });*/

                }else{

                    if (new ConnectionDetector(LoginActivity.this).isConnectingToInternet()) {

                        loginHandler.sendEmptyMessage(ThreadDefine.LOGIN);
                        /*if (checkPlayServices()) {
                            registerInBackground();
                        }*/
                    }
                    else {

                        progressBarCircular.setVisibility(View.GONE);

                        matDialog = new MaterialDialog(this);

                        matDialog.setMessage(getResources().getString(R.string.no_internet)).setPositiveButton(getResources().getString(R.string.ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                matDialog.dismiss();
                            }
                        }).show();

                    }


                }


                break;
        }
      }
    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            protected void onPreExecute() {
                super.onPreExecute();
                if (progressBarCircular != null) {
                    progressBarCircular.setVisibility(View.VISIBLE);
                    progressBarCircular.bringToFront();
                }
            }
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (googleCloudMessaging == null) {
                        googleCloudMessaging = GoogleCloudMessaging
                                .getInstance(LoginActivity.this);
                    }
                    ThreadDefine.GOOGLE_REG_ID = googleCloudMessaging
                            .register(getString(R.string.google_sender_id));
                    Log.d("Google Id :",""+ThreadDefine.GOOGLE_REG_ID);

                } catch (IOException ex) {
                    Log.d("Google IO Exception : ",""+ex.toString());
                    ThreadDefine.GOOGLE_REG_ID = "";
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                if (progressBarCircular != null)
                    progressBarCircular.setVisibility(View.GONE);

                if (!TextUtils.isEmpty(ThreadDefine.GOOGLE_REG_ID)) {
                    Toast.makeText(
                            LoginActivity.this,
                            "Registered with GCM Server successfully.nn"
                                    + msg, Toast.LENGTH_SHORT).show();
                    loginHandler.sendEmptyMessage(ThreadDefine.LOGIN);

                } else {
                    Toast.makeText(
                            LoginActivity.this,
                            "Reg ID Creation Failed.\nEither you haven't enabled Internet or GCM server is busy right now. Make sure you enabled Internet and try registering again after some time."
                                    + msg, Toast.LENGTH_LONG).show();
                }
            }
        }.execute(null, null, null);
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(LoginActivity.this,"This device doesn't support Play services, App will not work normally",
                        Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    private class AsyncDownloader extends AsyncTask<String, Integer, String> {

        //private String URL = "";
        private String filePath ="";
        private File destFile;
        private int downloaded = 0;
        private int urlLen = 0 ;
        public AsyncDownloader(){
            //URL = url;


        }
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressBarCircular != null) {
                progressBarCircular.setVisibility(View.VISIBLE);
                progressBarCircular.bringToFront();
            }
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setTitle("Downloading Documents");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setIcon(R.drawable.ico_download);
            progressDialog.setCancelable(true);
            progressDialog.show();

        }
        @Override
        protected String doInBackground(String... urls) {
            filePath = Environment.getExternalStorageDirectory().toString() + getString(R.string.folder_downloads);
            File folder = new File(filePath);

            if (!folder.exists())
                folder.mkdirs();

            int isInterrupt = 0;
            String url = apkDownloadUrl;

                    if (downloadPdfFile(url, urlLen) == 1)
                        isInterrupt = 1;

            if(isInterrupt==0)
                return "true";
            else
                return "false";
        }

        private int downloadPdfFile(String url,int urlLen){
            Log.d("No :"+urlLen+": Url "+url,"");
            int isInterrupt = 0;

            if (url.length() > 0) {
                OutputStream out = null;
                OkHttpClient httpClient = new OkHttpClient();
                Call call = httpClient.newCall(new Request.Builder().url(url).get().build());
                try {
                    String filename = url.substring(url.lastIndexOf("/"));
                    destFile = new File(filePath + filename);
                    if (destFile.exists()) {
                        if (destFile.canRead() && destFile.canWrite()) {
                            downloaded = (int) destFile.length();
                        } else {
                            destFile.delete();
                            downloaded = 0;
                        }
                    }
                    out = downloaded == 0 ? new FileOutputStream(destFile) : new FileOutputStream(destFile, true);

                    Response response = call.execute();
                    if (response.code() == 200) {
                        InputStream inputStream = null;
                        try {
                            inputStream = response.body().byteStream();
                            byte[] buff = new byte[1024 * 4];
                            long downloaded = 0;
                            long target = response.body().contentLength();
                            int length = 0;
                            while ((length = inputStream.read(buff)) > 0) {
                                downloaded += length;
                                publishProgress((int) ((downloaded * 100) / target),urlLen);
                                out.write(buff, 0, length);
                            }
                        } catch (IOException ignore) {
                            // return "false";
                            isInterrupt = 1;
                        } finally {
                            if (inputStream != null) {
                                inputStream.close();
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    isInterrupt = 1;
                }
            }
            return isInterrupt;
        }
        protected void onProgressUpdate(Integer... values) {
           /* downloadProgressBar.setMax(values[1].intValue());
            downloadProgressBar.setProgress(values[0].intValue());*/
            progressDialog.setProgressNumberFormat(null);
            progressDialog.setProgress(values[0]);

        }

        @Override
        protected void onPostExecute(String result) {
            // textViewStatus.setText(result ? "Downloaded" : "Failed");
            //downloadProgressBar.setVisibility(View.GONE);
            progressDialog.dismiss();
            //if(result.equals("true"))
               // newsLetterHandler.sendEmptyMessage(ThreadDefine.SYNC_DATA_SUCCESS);
           // else
               // newsLetterHandler.sendEmptyMessage(ThreadDefine.SYNC_DATA_FAILED);


        }
    }
    private boolean checkHigherVerison(){

        PackageInfo currentVerison =  ServiceEngUtil.getCurrentVersion(getApplicationContext());

        return versionCheck > currentVerison.versionCode;

    }
  }
