package com.ivalue.serviceeng.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ivalue.serviceeng.R;
import com.ivalue.serviceeng.adapter.ComplaintsAdapter;
import com.ivalue.serviceeng.bean.Constants;
import com.ivalue.serviceeng.bean.ThreadDefine;
import com.ivalue.serviceeng.dao.DAOFactory;
import com.ivalue.serviceeng.dao.ComplaintsDAO;
import com.ivalue.serviceeng.dto.ComplaintsDTO;
import com.ivalue.serviceeng.util.MaterialDialog;
import com.ivalue.serviceeng.util.ProgressBarCircular;

import java.util.ArrayList;

/**
 * Created by Sasikumar on 5/16/2016.
 */
public class ComplaintsFragment extends Fragment {

    private ProgressBarCircular progressBarCircular;

    private MaterialDialog matDialog;

    private ActionBar actionBar;

    private Activity thisActivity;

    //private ErrorSubTypeTask errorSubTypeTask;

    private ComplaintsAdapter complaintsAdapter;

    private ArrayList<ComplaintsDTO> errorSubTypeDTOArrayList;

    private Resources resources;

    private TextView noMoreNews;

    private ListView complaintsListView;

    private SharedPreferences dashboardSharedPref;

    private int productId = 0,complaintsId=0;

    private Bundle bundle;

    private String productType= "";

    private TextView headerTxt;

    private Context mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.receive_communication_fragment, container,
                false);
        try {

            thisActivity = getActivity();
            matDialog = new MaterialDialog(thisActivity);
            bundle = getArguments();
            mContext = thisActivity.getApplicationContext();
            if(bundle.containsKey("product_id"))
                productId = bundle.getInt("product_id");

            if(bundle.containsKey("product_type"))
                productType = bundle.getString("product_type");

            progressBarCircular = (ProgressBarCircular) rootView.findViewById(R.id.product_progress);

            progressBarCircular.setVisibility(View.GONE);

            resources = thisActivity.getResources();
            dashboardSharedPref = thisActivity.getSharedPreferences(ThreadDefine.SHARED_PREF_NAME, Context.MODE_PRIVATE);

            headerTxt = (TextView) rootView.findViewById(R.id.title_txt);

            headerTxt.setText(resources.getString(R.string.compliants)+"-"+productType);

            if(ThreadDefine.AUTHENTICATION_TOKEN.length()==0)
                ThreadDefine.AUTHENTICATION_TOKEN  = dashboardSharedPref.getString(Constants.AUTH_TOKEN,"");

            noMoreNews = (TextView) rootView.findViewById(R.id.no_news_letter_txt);

            complaintsListView = (ListView) rootView.findViewById(R.id.news_letter_list);

            errorSubTypeDTOArrayList = new ArrayList<ComplaintsDTO>();

            complaintsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                    complaintsId = errorSubTypeDTOArrayList.get(position).getComplaintsTypeId();
                    String complaintsType = errorSubTypeDTOArrayList.get(position).getComplaintsType();
                    String definition = errorSubTypeDTOArrayList.get(position).getDefinition();
                    String confirmCheckPoints = errorSubTypeDTOArrayList.get(position).getConfirmCheckPoint();
                    Bundle errorBundle = new Bundle();
                    errorBundle.putInt("product_id", productId);
                    errorBundle.putInt("complaints_id", complaintsId);
                    errorBundle.putString("complaints_type", complaintsType);
                    errorBundle.putString("definition", definition);
                    errorBundle.putString("confirm_check_point", confirmCheckPoints);

                    ((DashboardActivity) thisActivity).selectPageFragment(ThreadDefine.GET_COMPLAINTS_CONFIRMATION, errorBundle);
                }

            });

            ComplaintsDAO errorSubTypeSqlDAO = (ComplaintsDAO) DAOFactory.createObjectDAO(thisActivity, ComplaintsDTO.class);

            errorSubTypeDTOArrayList = errorSubTypeSqlDAO.findGroupsById(productId);

            if(errorSubTypeDTOArrayList.size()>0) {

                complaintsAdapter = new ComplaintsAdapter(thisActivity, errorSubTypeDTOArrayList);

                complaintsListView.setAdapter(complaintsAdapter);

                complaintsAdapter.notifyDataSetChanged();

                noMoreNews.setVisibility(View.GONE);

                complaintsListView.setVisibility(View.VISIBLE);

            }else{

                headerTxt.setVisibility(View.GONE);

                noMoreNews.setText(resources.getString(R.string.no_data_show));

                noMoreNews.setVisibility(View.VISIBLE);

                complaintsListView.setVisibility(View.GONE);
            }
            /*if (new ConnectionDetector(thisActivity).isConnectingToInternet()) {

                newsLetterHandler.sendEmptyMessage(ThreadDefine.GET_ERROR_SUB_TYPE);

            }else {

                matDialog = new MaterialDialog(thisActivity);

                matDialog.setMessage(resources.getString(R.string.no_internet)).setPositiveButton(resources.getString(R.string.ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        matDialog.dismiss();

                    }
                }).show();
            }*/

        } catch (Exception ex) {
            Log.d("", "" + ex.toString());
        }
        return rootView;

    }


    public Handler newsLetterHandler = new Handler(new Handler.Callback() {


        public boolean handleMessage(Message msg) {

            switch (msg.what) {

               /* case ThreadDefine.GET_ERROR_SUB_TYPE:

                    errorSubTypeTask = new ErrorSubTypeTask();

                    errorSubTypeTask.execute(msg.what);

                    break;*/

                case ThreadDefine.GET_ERROR_SUB_TYPE_SUCCESS:

                    if(errorSubTypeDTOArrayList.size()>0) {

                        complaintsAdapter = new ComplaintsAdapter(thisActivity, errorSubTypeDTOArrayList);

                        complaintsListView.setAdapter(complaintsAdapter);

                        complaintsAdapter.notifyDataSetChanged();

                        noMoreNews.setVisibility(View.GONE);

                        complaintsListView.setVisibility(View.VISIBLE);

                    }else{

                        noMoreNews.setVisibility(View.VISIBLE);

                        complaintsListView.setVisibility(View.GONE);
                    }


                    break;
                case ThreadDefine.NO_MORE_ERROR_CODE:
                    noMoreNews.setVisibility(View.VISIBLE);
                    noMoreNews.setText("No more records to show");
                    complaintsListView.setVisibility(View.GONE);

                    break;

                case ThreadDefine.GET_ERROR_SUB_TYPE_FAILED:

                    progressBarCircular.setVisibility(View.INVISIBLE);

                    matDialog = new MaterialDialog(thisActivity);

                    matDialog.setMessage(resources.getString(R.string.server_not_resp)).setPositiveButton(resources.getString(R.string.ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            matDialog.dismiss();

                        }
                    }).show();

                    break;
            }

            return  true;
        }
    });


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        // Implementing ActionBar Search inside a fragment
        MenuItem item = menu.add("Search");
        item.setIcon(R.drawable.ico_search); // sets icon
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        SearchView sv = new SearchView(thisActivity);

        // modifying the text inside edittext component
        //int id = sv.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        //TextView textView = (TextView) sv.findViewById(id);
        //textView.setHint("Search location...");
        //textView.setHintTextColor(getResources().getColor(R.color.DarkGray));
       // textView.setTextColor(getResources().getColor(R.color.clouds));

        // implementing the listener
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if (s.length() < 4) {
                    Toast.makeText(getActivity(),
                            "Your search query must not be less than 3 characters",
                            Toast.LENGTH_LONG).show();
                    return true;
                } else {
                  //  doSearch(s);
                    return false;
                }
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
        item.setActionView(sv);
    }

   /* @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.search_menu, menu);
        //MenuItem item = menu.findItem(R.id.action_search);
        //SearchView searchView = new SearchView(((DashboardActivity) thisActivity).getActionBar().getThemedContext());

        MenuItem item = menu.findItem(R.id.action_search);
        //SearchView searchView = (SearchView) item.getActionView();
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        //MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
       // MenuItemCompat.setActionView(item, searchView);


       if(searchView!=null) {
           searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
               @Override
               public boolean onQueryTextSubmit(String query) {
                   return false;
               }

               @Override
               public boolean onQueryTextChange(String newText) {

                   return false;

               }
           });
           searchView.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View v) {

                                             }
                                         }
           );
       }

    }
*/
   /* private class ErrorSubTypeTask extends AsyncTask<Integer, Integer, Integer>
    {
        int result = 0;

        protected void onPreExecute() {
            super.onPreExecute();
            if (progressBarCircular != null) {
                progressBarCircular.setVisibility(View.VISIBLE);
                progressBarCircular.bringToFront();
            }
        }


        @Override
        protected Integer doInBackground(Integer... params) {
            switch (params[0]) {
                case ThreadDefine.GET_ERROR_SUB_TYPE:
                    try {
                        OkHttpClient client = new OkHttpClient();
                        JSONObject productJson = new JSONObject();
                        MediaType JSON
                                = MediaType.parse("application/json; charset=utf-8");

                        Request request = new Request.Builder()
                                .url(resources.getString(R.string.app_url)+ "error_subtypes/auth_token="+ThreadDefine.AUTHENTICATION_TOKEN+"&err_typeid="+errorTypeId)
                                .build();

                        Response response = client.newCall(request).execute();

                        JSONObject responseJsonObj = new JSONObject(response.body().string());

                        if(responseJsonObj.has("status_code")) {

                            int statusCode = responseJsonObj.getInt("status_code");

                            if(statusCode==200){

                                if(responseJsonObj.has("error_subtypes")){

                                    JSONArray errorSubTypeJSonObj = responseJsonObj.getJSONArray("error_subtypes");

                                    //  Iterator<String> newsIteration = newsLetterJSonObj.keys();

                                    int commLen = errorSubTypeJSonObj.length();

                                    errorSubTypeDTOArrayList = new ArrayList<ComplaintsDTO>();

                                    for(int comm=0;comm<commLen;comm++) {

                                        JSONObject newsLetterJSon =  errorSubTypeJSonObj.getJSONObject(comm);

                                        ComplaintsDTO newsLetterDTO = new ComplaintsDTO();

                                        if(!newsLetterJSon.isNull("error_subtype"))
                                            newsLetterDTO.setErrorSubType(newsLetterJSon.getString("error_subtype"));

                                        if(!newsLetterJSon.isNull("error_subtypeid"))
                                            newsLetterDTO.setErrorSubId(newsLetterJSon.getInt("error_subtypeid"));


                                        errorSubTypeDTOArrayList.add(newsLetterDTO);


                                    }

                                    result = ThreadDefine.GET_ERROR_SUB_TYPE_SUCCESS;
                                }else
                                    result = ThreadDefine.GET_ERROR_SUB_TYPE_FAILED;


                            }else if(statusCode ==300)
                                result = ThreadDefine.NO_MORE_ERROR_CODE;
                            else
                                result = ThreadDefine.GET_ERROR_SUB_TYPE_FAILED;

                        }else
                            result = ThreadDefine.GET_ERROR_SUB_TYPE_FAILED;




                    }catch (Exception ex){

                        result = ThreadDefine.GET_ERROR_SUB_TYPE_FAILED;

                        Log.d("","Receive Comm Task Exception "+ex.toString());
                    }

                    break;


            }

            return result;
        }

        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);

            if (progressBarCircular != null)
                progressBarCircular.setVisibility(View.GONE);

            newsLetterHandler.sendEmptyMessage(result);
        }
    }*/
}