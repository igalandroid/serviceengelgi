package com.ivalue.serviceeng.ui;

import android.app.ActionBar;
import android.content.res.Resources;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.widget.ListView;

import com.ivalue.serviceeng.adapter.NavigationDrawerAdapter;
import com.ivalue.serviceeng.util.MaterialDialog;
import com.ivalue.serviceeng.util.ldrawer.ActionBarDrawerToggle;
import com.ivalue.serviceeng.util.ldrawer.DrawerArrowDrawable;

/**
 * Created by Sasikumar on 9/10/2016.
 */
public class HomeActivity extends FragmentActivity {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerArrowDrawable drawerArrow;
    private boolean drawerArrowColor;
    private String[] navigationArrStr;
    private Resources resources;
    private ActionBar actionBar;
    private NavigationDrawerAdapter navigationDrawer;
    private int[] imageResId;
    private MaterialDialog matDialog;

}
