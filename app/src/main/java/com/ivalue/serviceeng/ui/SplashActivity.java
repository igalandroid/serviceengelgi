package com.ivalue.serviceeng.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.ivalue.serviceeng.R;
import com.ivalue.serviceeng.bean.Constants;
import com.ivalue.serviceeng.bean.ThreadDefine;
import com.ivalue.serviceeng.util.MaterialDialog;

/*
 */
public class SplashActivity extends Activity {

    private int SPLASH_TIME_OUT = 0;

    private SharedPreferences humauraPrefs = null;

    private boolean firstRun = false;

    private  SharedPreferences splashSharedPref;

    private String authToken ="";

    LocationManager locationManager ;

    boolean gps_enabled = false;

    boolean network_enabled = false;

    private MaterialDialog matDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.splash_activity);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        splashSharedPref = getSharedPreferences(ThreadDefine.SHARED_PREF_NAME, Context.MODE_PRIVATE);


        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        try {

            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        } catch(Exception ex) {


        }

        try {

            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        } catch(Exception ex) {


        }

        authToken = splashSharedPref.getString(Constants.AUTH_TOKEN, "");

        SPLASH_TIME_OUT = Integer.parseInt(getString(R.string.splash_timeout));

       /* if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

            matDialog = new MaterialDialog(this);

            matDialog.setMessage(getResources().getString(R.string.enable_gps_device)).setPositiveButton(getResources().getString(R.string.enable_gps), new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    matDialog.dismiss();

                    Intent callGPSSettingIntent = new Intent(
                            android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(callGPSSettingIntent);


                }
            }).setNegativeButton(getResources().getString(R.string.cancel), new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    matDialog.dismiss();

                    finish();

                }
            }).show();

        }else {

            checkAuthToken();
        }*/

        //checkLocation();

    }

    public void onResume() {
        super.onResume();

        //checkLocation();
        checkAuthToken();

    }

    private void checkLocation() {

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

            matDialog = new MaterialDialog(this);

            matDialog.setMessage(getResources().getString(R.string.enable_gps_device)).setPositiveButton(getResources().getString(R.string.enable_gps), new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    matDialog.dismiss();

                    Intent callGPSSettingIntent = new Intent(
                            android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(callGPSSettingIntent);


                }
            }).setNegativeButton(getResources().getString(R.string.cancel), new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    matDialog.dismiss();

                    finish();

                }
            }).show();

        }else {

            checkAuthToken();
        }
    }

    private void checkAuthToken() {

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {


                if(authToken.length()>0)
                    startActivity(new Intent(SplashActivity.this, DashboardActivity.class));
                else
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));

                finish();
            }
        }, SPLASH_TIME_OUT);

    }
}




