package com.ivalue.serviceeng.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.ivalue.serviceeng.R;
import com.ivalue.serviceeng.adapter.ProductGroupAdapter;
import com.ivalue.serviceeng.bean.Constants;
import com.ivalue.serviceeng.bean.ThreadDefine;
import com.ivalue.serviceeng.dao.DAOFactory;
import com.ivalue.serviceeng.dao.ProductGroupDAO;
import com.ivalue.serviceeng.dto.ProductGroupDTO;
import com.ivalue.serviceeng.util.MaterialDialog;
import com.ivalue.serviceeng.util.ProgressBarCircular;

import java.util.ArrayList;

/**
 * Created by Sasikumar on 5/16/2016.
 */
public class ProductGroupListFragment extends Fragment {

    private ProgressBarCircular progressBarCircular;

    private MaterialDialog matDialog;

    private ActionBar actionBar;

    private Activity thisActivity;

    private ProductGroupAdapter productGroupAdapter;

    private ArrayList<ProductGroupDTO> errorTypeDTOArrayList;

    private Resources resources;

    private TextView noMoreNews;

    private ListView productGroupListView;

    private SharedPreferences dashboardSharedPref;

    private TextView headerTxt;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View rootView = inflater.inflate(R.layout.error_type_fragment, container,
                false);
        try {

            thisActivity = getActivity();

            matDialog = new MaterialDialog(thisActivity);
            progressBarCircular = (ProgressBarCircular) rootView.findViewById(R.id.product_progress);
            progressBarCircular.setVisibility(View.GONE);

            resources = thisActivity.getResources();
            dashboardSharedPref = thisActivity.getSharedPreferences(ThreadDefine.SHARED_PREF_NAME, Context.MODE_PRIVATE);

            if(ThreadDefine.AUTHENTICATION_TOKEN.length()==0)
                ThreadDefine.AUTHENTICATION_TOKEN  = dashboardSharedPref.getString(Constants.AUTH_TOKEN,"");

            noMoreNews = (TextView) rootView.findViewById(R.id.no_news_letter_txt);

            headerTxt = (TextView) rootView.findViewById(R.id.title_txt);

            headerTxt.setText(resources.getString(R.string.product_group));

            productGroupListView = (ListView) rootView.findViewById(R.id.error_type_list);

            productGroupListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                    // if(errorTypeDTOArrayList.size()>0){
                    int errorId = errorTypeDTOArrayList.get(position).getProductId();
                    String productType = errorTypeDTOArrayList.get(position).getProductType();
                    Bundle errorBundle = new Bundle();
                    errorBundle.putInt("product_id", errorId);
                    errorBundle.putString("product_type", productType);

                    ((DashboardActivity) thisActivity).selectPageFragment(ThreadDefine.GET_COMPLAINTS_TYPE, errorBundle);
                    //}
                }
            });

            ProductGroupDAO errorTypeSqlDAO = (ProductGroupDAO) DAOFactory.createObjectDAO(thisActivity, ProductGroupDTO.class);

            errorTypeDTOArrayList = errorTypeSqlDAO.getAllErrorType();

            if(errorTypeDTOArrayList.size()>0) {
                productGroupAdapter = new ProductGroupAdapter(thisActivity, errorTypeDTOArrayList);
                productGroupListView.setAdapter(productGroupAdapter);

                productGroupAdapter.notifyDataSetChanged();

                noMoreNews.setVisibility(View.GONE);

                productGroupListView.setVisibility(View.VISIBLE);

            }else{

                noMoreNews.setText(resources.getString(R.string.no_data_show));

                noMoreNews.setVisibility(View.VISIBLE);

                productGroupListView.setVisibility(View.GONE);
            }

           /* if (new ConnectionDetector(thisActivity).isConnectingToInternet()) {
                newsLetterHandler.sendEmptyMessage(ThreadDefine.GET_ERROR_TYPE);
            }else {
                matDialog = new MaterialDialog(thisActivity);
                matDialog.setMessage(resources.getString(R.string.no_internet)).setPositiveButton(resources.getString(R.string.ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        matDialog.dismiss();
                    }
                }).show();
            }*/
        } catch (Exception ex) {
            Log.d("", "" + ex.toString());
        }
        return rootView;
    }


    public Handler newsLetterHandler = new Handler(new Handler.Callback() {


        public boolean handleMessage(Message msg) {

            switch (msg.what) {

               /* case ThreadDefine.GET_ERROR_TYPE:

                    errorTypeTask = new ReceiveCommTask();

                    errorTypeTask.execute(msg.what);

                    break;*/

                case ThreadDefine.GET_ERROR_TYPE_SUCCESS:

                    if(errorTypeDTOArrayList.size()>0) {

                        productGroupAdapter = new ProductGroupAdapter(thisActivity, errorTypeDTOArrayList);

                        productGroupListView.setAdapter(productGroupAdapter);

                        productGroupAdapter.notifyDataSetChanged();

                        noMoreNews.setVisibility(View.GONE);

                        productGroupListView.setVisibility(View.VISIBLE);

                    }else{

                        noMoreNews.setVisibility(View.VISIBLE);

                        productGroupListView.setVisibility(View.GONE);
                    }

                    break;
                case ThreadDefine.NO_MORE_ERROR_CODE:
                    noMoreNews.setVisibility(View.VISIBLE);
                    noMoreNews.setText("No more records to show");
                    productGroupListView.setVisibility(View.GONE);

                    break;

                case ThreadDefine.GET_ERROR_TYPE_FAILED:

                    progressBarCircular.setVisibility(View.INVISIBLE);

                    matDialog = new MaterialDialog(thisActivity);

                    matDialog.setMessage(resources.getString(R.string.server_not_resp)).setPositiveButton(resources.getString(R.string.ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            matDialog.dismiss();

                        }
                    }).show();


                    break;


            }

            return  true;
        }
    });

    /*private class ReceiveCommTask extends AsyncTask<Integer, Integer, Integer>

    {
        int result = 0;

        protected void onPreExecute() {

            super.onPreExecute();

            if (progressBarCircular != null) {

                progressBarCircular.setVisibility(View.VISIBLE);

                progressBarCircular.bringToFront();

            }

        }


        @Override
        protected Integer doInBackground(Integer... params) {

            switch (params[0]) {

                case ThreadDefine.GET_ERROR_TYPE:

                    try {

                        OkHttpClient client = new OkHttpClient();

                        JSONObject productJson = new JSONObject();



                        MediaType JSON
                                = MediaType.parse("application/json; charset=utf-8");


                        Request request = new Request.Builder()
                                .url(resources.getString(R.string.app_url)+ "error_types/auth_token="+ThreadDefine.AUTHENTICATION_TOKEN)
                                .build();

                       *//* Request request = new Request.Builder()
                                .url("http://www.mocky.io/v2/55b3a22f1c5bf17a04c90066")
                                .build();*//*

                        Response response = client.newCall(request).execute();

                        JSONObject responseJsonObj = new JSONObject(response.body().string());

                        if(responseJsonObj.has("status_code")) {

                            int statusCode = responseJsonObj.getInt("status_code");

                            if(statusCode==200){

                                if(responseJsonObj.has("error_types")){

                                    JSONArray newsLetterJSonObj = responseJsonObj.getJSONArray("error_types");

                                    //  Iterator<String> newsIteration = newsLetterJSonObj.keys();

                                    int commLen = newsLetterJSonObj.length();

                                    errorTypeDTOArrayList = new ArrayList<ProductGroupDTO>();

                                    for(int comm=0;comm<commLen;comm++) {

                                        JSONObject newsLetterJSon =  newsLetterJSonObj.getJSONObject(comm);

                                        ProductGroupDTO newsLetterDTO = new ProductGroupDTO();

                                        if(!newsLetterJSon.isNull("error_type"))
                                            newsLetterDTO.setProductType(newsLetterJSon.getString("error_type"));

                                        if(!newsLetterJSon.isNull("error_typeid"))
                                            newsLetterDTO.setProductId(newsLetterJSon.getInt("error_typeid"));



                                        errorTypeDTOArrayList.add(newsLetterDTO);


                                    }

                                    result = ThreadDefine.GET_ERROR_TYPE_SUCCESS;
                                }else
                                    result = ThreadDefine.GET_ERROR_TYPE_FAILED;


                            }else if(statusCode ==300)
                                result = ThreadDefine.NO_MORE_ERROR_CODE;

                            else
                                result = ThreadDefine.GET_ERROR_TYPE_FAILED;

                        }else
                            result = ThreadDefine.GET_ERROR_TYPE_FAILED;




                    }catch (Exception ex){

                        result = ThreadDefine.GET_ERROR_TYPE_FAILED;

                        Log.d("","Receive Comm Task Exception "+ex.toString());
                    }

                    break;


            }

            return result;
        }

        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);

            if (progressBarCircular != null)
                progressBarCircular.setVisibility(View.GONE);

            newsLetterHandler.sendEmptyMessage(result);
        }
    }*/
}