package com.ivalue.serviceeng.ui;

/**
 * Created by  on 7/7/15.
 */

import android.accounts.AccountManager;
import android.app.ActionBar;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.ivalue.serviceeng.R;
import com.ivalue.serviceeng.adapter.NavigationDrawerAdapter;
import com.ivalue.serviceeng.bean.Constants;
import com.ivalue.serviceeng.bean.ThreadDefine;
import com.ivalue.serviceeng.location.SecurityTokenExpiredListener;
import com.ivalue.serviceeng.util.MaterialDialog;

public class DashboardActivity extends FragmentActivity {

    private DrawerLayout drawerLayout;

    private ListView drawerList;

    private ActionBarDrawerToggle drawerToggle;

    private ActionBar actionBar;

    private MaterialDialog matDialog;

    private RelativeLayout drawerListRel;

    private Resources resources;

    private String[] navigationArrStr;

    private NavigationDrawerAdapter navigationDrawer;

    private  Fragment errorTypeListFragment,complaintsConfirmationFragment, complaintsFragment,errorCodeFragment,errorCodificationFragment,dataSyncFragment;

    private TextView titleActionBarTxt;

    private SharedPreferences dashBoardSharedPref;

    private int[] imageResId;

    AccountManager accountManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dashboard_activity);

       /* this.overridePendingTransition(R.animator.animation_enter,
                R.animator.animation_leave);*/

        try {

            actionBar = getActionBar();

            matDialog = new MaterialDialog(this);

            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

            resources = getResources();

            dashBoardSharedPref = getSharedPreferences(ThreadDefine.SHARED_PREF_NAME, Context.MODE_PRIVATE);

            if(ThreadDefine.AUTHENTICATION_TOKEN.length()==0)
                ThreadDefine.AUTHENTICATION_TOKEN  = dashBoardSharedPref.getString(Constants.AUTH_TOKEN,"");

            if (actionBar != null) {



                    LayoutInflater inflator = (LayoutInflater) this
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View view = inflator.inflate(R.layout.action_bar_title, null);

                    titleActionBarTxt = (TextView) view.findViewById(R.id.title);

                    actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);

                actionBar.setCustomView(view, new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        ActionBar.LayoutParams.MATCH_PARENT,
                        Gravity.CENTER
                ));

                actionBar. setBackgroundDrawable (new ColorDrawable(resources.getColor(R.color.bar_red)));


                   /* actionBar.setDisplayHomeAsUpEnabled(true);

                    actionBar.setHomeButtonEnabled(true);

                    actionBar.setDisplayShowHomeEnabled(false);

                    actionBar.setDisplayShowTitleEnabled(false);*/

                actionBar.setIcon(android.R.color.transparent);

                    // actionBar.setIcon(R.drawable.logo);



            }

            setNavigationDrawer();

            selectedItem(0);


        } catch (Exception ex) {

            Log.d("---Dashboard-"+ex.toString(),"------"+ex.toString());

        }

    }

    public void setNavigationDrawer() {

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        drawerListRel = (RelativeLayout) findViewById(R.id.drawer_view);

        drawerList = (ListView) findViewById(R.id.menu_lst);


        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,

                R.drawable.ic_drawer, R.string.drawer_open,
                R.string.drawer_close) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }
        };

        drawerToggle.setDrawerIndicatorEnabled(true);

        drawerLayout.setDrawerListener(drawerToggle);

        drawerToggle.syncState();

        navigationArrStr = new String[]{

                resources.getString(R.string.troubleshoot),
                resources.getString(R.string.sync_data),
                resources.getString(R.string.logout)

        };
        /*

         R.drawable.ico_customer_details_active,
                R.drawable.ico_receive_communication_active,
                R.drawable.ico_send_communication_active,
                R.drawable.ico_service_report_active,

        resources.getString(R.string.customer_details),
                resources.getString(R.string.receive_com),
                resources.getString(R.string.send_com),
                resources.getString(R.string.service_report),
         */

        imageResId = new int[] {
                R.drawable.ico_troubleshoot,
                R.drawable.ico_sync,
                R.drawable.ico_logout_active


        };

        navigationDrawer = new NavigationDrawerAdapter(
                this, R.layout.navigation_row, navigationArrStr,imageResId);

        drawerList.setAdapter(navigationDrawer);

        navigationDrawer.notifyDataSetChanged();

        drawerLayout.invalidate();

        drawerList.setOnItemClickListener(new SlideMenuClickListener());
  }



    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {

        private View lastSelectedView = null;

        public void clearSelection()
        {
            if(lastSelectedView != null) lastSelectedView.setBackgroundColor(resources.getColor(R.color.blue_nav_bar));
        }


        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {

            clearSelection();

            lastSelectedView = view;

            view.setBackgroundColor(resources.getColor(R.color.blue_sel_list));

            if (drawerLayout.isDrawerOpen(drawerListRel)) {
                drawerLayout.closeDrawer(drawerListRel);
            } else {
                drawerLayout.openDrawer(drawerListRel);
            }



            selectedItem(position);
        }
    }

    private void storeSharedPref() {

        if(dashBoardSharedPref!=null) {

            SharedPreferences.Editor loginEditor = dashBoardSharedPref.edit();

            loginEditor.putString(Constants.AUTH_TOKEN, "");

            loginEditor.putString(Constants.USER_ID,"");

            //loginEditor.putString(Constants.EMP_NAME,ThreadDefine.EMP_NAME);

            loginEditor.commit();

        }


    }

    private void selectedItem(int position){


        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        switch (position) {

            /*case 0:

                titleActionBarTxt.setText(resources.getString(R.string.customer_details));

                customerDetailsFragment = new CustomerListFragment();

                fragmentTransaction
                        .replace(R.id.content_frame, customerDetailsFragment)
                        .commit();



            break;

            case 1:

                titleActionBarTxt.setText(resources.getString(R.string.receive_com));

                recieveComFragment = new ReceiveCommunicationFragment();

                fragmentTransaction
                        .replace(R.id.content_frame, recieveComFragment)
                        .commit();

            break;

            case 2:

                titleActionBarTxt.setText(resources.getString(R.string.send_com));

                sendComFragment = new SendCommunicationFragment();

                fragmentTransaction
                        .replace(R.id.content_frame, sendComFragment)
                        .commit();

            break;


            case 3:

                titleActionBarTxt.setText(resources.getString(R.string.service_report));

                serviceReportFragment = new ServiceReportFragment();

                fragmentTransaction
                        .replace(R.id.content_frame, serviceReportFragment)
                        .commit();

            break;*/
            case 0:

                titleActionBarTxt.setText(resources.getString(R.string.troubleshoot));
                errorTypeListFragment = new ProductGroupListFragment();
                fragmentTransaction
                        .replace(R.id.content_frame, errorTypeListFragment)
                        .commit();
            break;
            case 1:
                titleActionBarTxt.setText(resources.getString(R.string.sync_data));
                dataSyncFragment = new SyncDataFragment();
                fragmentTransaction
                        .replace(R.id.content_frame, dataSyncFragment)
                        .commit();
            break;
            case 2:

                storeSharedPref();
                ThreadDefine.AUTHENTICATION_TOKEN = "";
                Intent logoutInt = new Intent(DashboardActivity.this,LoginActivity.class);
                startActivity(logoutInt);
                this.finish();


            break;
        }

    }

    public void selectPageFragment(int selectedPage,Bundle bundleData){

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        switch(selectedPage){

            case ThreadDefine.GET_COMPLAINTS_CONFIRMATION:

                titleActionBarTxt.setText(resources.getString(R.string.troubleshoot));
                complaintsConfirmationFragment = new ComplaintConfirmationFragment();
                complaintsConfirmationFragment.setArguments(bundleData);
                fragmentTransaction
                        .replace(R.id.content_frame, complaintsConfirmationFragment)
                        .addToBackStack(null)
                        .commit();

              break;

            case ThreadDefine.GET_COMPLAINTS_TYPE:

                titleActionBarTxt.setText(resources.getString(R.string.troubleshoot));
                complaintsFragment = new ComplaintsFragment();
                complaintsFragment.setArguments(bundleData);
                fragmentTransaction
                        .replace(R.id.content_frame, complaintsFragment)

                    .addToBackStack(null)
                    .commit();

            break;

            case ThreadDefine.GET_CHECK_POINTS_TYPE:

                titleActionBarTxt.setText(resources.getString(R.string.troubleshoot));
                errorCodeFragment = new CheckPointsFragment();
                errorCodeFragment.setArguments(bundleData);
                fragmentTransaction
                        .replace(R.id.content_frame, errorCodeFragment)
                        .addToBackStack(null)
                        .commit();

                break;
            case ThreadDefine.GET_CHECK_PROCEDURE:

                titleActionBarTxt.setText(resources.getString(R.string.troubleshoot));
                errorCodificationFragment = new CheckProcedureFragment();
                errorCodificationFragment.setArguments(bundleData);
                fragmentTransaction
                        .replace(R.id.content_frame, errorCodificationFragment)
                        .addToBackStack(null)
                        .commit();
                break;

            case ThreadDefine.GET_PRODUCT_GROUP:

                titleActionBarTxt.setText(resources.getString(R.string.troubleshoot));
                errorTypeListFragment = new ProductGroupListFragment();
                fragmentTransaction
                        .replace(R.id.content_frame, errorTypeListFragment)
                        .commit();
                break;
        }

    }


    public void openDrawer() {

        if (drawerLayout.isDrawerOpen(drawerListRel)) {
            drawerLayout.closeDrawer(drawerListRel);
        } else {
            drawerLayout.openDrawer(drawerListRel);
        }
    }

    public void closeDrawer() {

        if (drawerLayout.isDrawerOpen(drawerListRel))
            drawerLayout.closeDrawer(drawerListRel);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

      /*  MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.dashboard_actions, menu);*/

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:


                openDrawer();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {

        if (drawerList != null) {

            drawerList.setAdapter(navigationDrawer);
            //drawerList.invalidate();

        }

        if (navigationDrawer != null)
            navigationDrawer.notifyDataSetChanged();

        if (drawerList != null)
            drawerList.invalidate();

        return super.onPrepareOptionsMenu(menu);

    }

    @Override
    protected void onPause() {
        super.onPause();

       // setSecurityTokenExpire();
    }

    private void setSecurityTokenExpire() {

        long securityTokenExpireTime =
                dashBoardSharedPref.getLong(Constants.SECURITY_TOKEN_EXPIRE_TIME,0L);

        Intent intent = new Intent(this, SecurityTokenExpiredListener.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                this.getApplicationContext(), 234324243, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, securityTokenExpireTime, pendingIntent);
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }
}
