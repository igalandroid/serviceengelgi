package com.ivalue.serviceeng;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.ivalue.serviceeng.location.LocationChangeService;

/**
 * Created by
 */
public class ServiceEngApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        startService(getApplicationContext());

    }

    private void startService(Context context) {

        Intent service = new Intent(context, LocationChangeService.class);

        context.startService(service);
    }
}
