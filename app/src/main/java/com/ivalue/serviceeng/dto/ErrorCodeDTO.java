package com.ivalue.serviceeng.dto;

/**
 * Created by Sasikumar on 5/17/2016.
 */
public class ErrorCodeDTO {

    private String errorCode="";
    private int errorCodeId=0;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public int getErrorCodeId() {
        return errorCodeId;
    }

    public void setErrorCodeId(int errorCodeId) {
        this.errorCodeId = errorCodeId;
    }
}
