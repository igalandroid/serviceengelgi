package com.ivalue.serviceeng.dto;

/**
 * Created by railsfactory on 24/8/15.
 */
public class EquipmentDTO {

    private String equipmentName = "";

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }
}
