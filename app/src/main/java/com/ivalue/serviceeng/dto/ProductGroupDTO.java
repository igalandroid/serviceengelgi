package com.ivalue.serviceeng.dto;

/**
 * Created by Sasikumar on 5/16/2016.
 */
public class ProductGroupDTO {
    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    private String productType="";
    private int productId=0;



}
