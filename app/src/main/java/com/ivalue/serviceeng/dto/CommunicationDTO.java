package com.ivalue.serviceeng.dto;

/**
 * Created by sasikumar.thangarasu on 2/20/2016.
 */
public class CommunicationDTO {

    public String getCommunicationDetails() {
        return communicationDetails;
    }

    public void setCommunicationDetails(String communicationDetails) {
        this.communicationDetails = communicationDetails;
    }

    public String getCommunicationTime() {
        return communicationTime;
    }

    public void setCommunicationTime(String communicationTime) {
        this.communicationTime = communicationTime;
    }

    public String getCommunicationTitle() {
        return communicationTitle;
    }

    public void setCommunicationTitle(String communicationTitle) {
        this.communicationTitle = communicationTitle;
    }

    public byte getCommunicaitonType() {
        return communicaitonType;
    }

    public void setCommunicaitonType(byte communicaitonType) {
        this.communicaitonType = communicaitonType;
    }

    private String communicationDetails= "",communicationTime="",communicationTitle="";

    private  byte communicaitonType;
}
