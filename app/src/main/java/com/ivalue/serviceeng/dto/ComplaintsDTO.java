package com.ivalue.serviceeng.dto;

/**
 * Created by Sasikumar on 5/16/2016.
 */
public class ComplaintsDTO {


    private String complaintsType="",confirmCheckPoint="",definition="";

    private int productId=0,complaintsTypeId=0;

    public String getComplaintsType() {
        return complaintsType;
    }

    public void setComplaintsType(String complaintsType) {
        this.complaintsType = complaintsType;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getComplaintsTypeId() {
        return complaintsTypeId;
    }

    public void setComplaintsTypeId(int complaintsTypeId) {
        this.complaintsTypeId = complaintsTypeId;
    }
    public String getConfirmCheckPoint() {
        return confirmCheckPoint;
    }

    public void setConfirmCheckPoint(String confirmCheckPoint) {
        this.confirmCheckPoint = confirmCheckPoint;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }





}
