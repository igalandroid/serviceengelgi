package com.ivalue.serviceeng.dto;

/**
 * Created by Sasikumar on 5/17/2016.
 */
public class CheckpointsDTO {
    public String getCheckImagePath() {
        return checkImagePath;
    }

    public void setCheckImagePath(String checkImagePath) {
        this.checkImagePath = checkImagePath;
    }

    public String getCheckImageName() {
        return checkImageName;
    }

    public void setCheckImageName(String checkImageName) {
        this.checkImageName = checkImageName;
    }

    private String checkingProcedure="",checkPointCode="",checkPointProbableRootCause="",checkPointSolution="",filePath="",fileName="",checkImagePath="",checkImageName="";
    private int checkPointCodeId=0,complaintsTypeId=0,productTypeId=0;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getCheckingProcedure() {

        return checkingProcedure;
    }

    public void setCheckingProcedure(String checkingProcedure) {
        this.checkingProcedure = checkingProcedure;
    }

    public String getCheckPointCode() {
        return checkPointCode;
    }

    public void setCheckPointCode(String checkPointCode) {
        this.checkPointCode = checkPointCode;
    }

    public String getCheckPointProbableRootCause() {
        return checkPointProbableRootCause;
    }

    public void setCheckPointProbableRootCause(String checkPointProbableRootCause) {
        this.checkPointProbableRootCause = checkPointProbableRootCause;
    }

    public String getCheckPointSolution() {
        return checkPointSolution;
    }

    public void setCheckPointSolution(String checkPointSolution) {
        this.checkPointSolution = checkPointSolution;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getCheckPointCodeId() {
        return checkPointCodeId;
    }

    public void setCheckPointCodeId(int checkPointCodeId) {
        this.checkPointCodeId = checkPointCodeId;
    }

    public int getComplaintsTypeId() {
        return complaintsTypeId;
    }

    public void setComplaintsTypeId(int complaintsTypeId) {
        this.complaintsTypeId = complaintsTypeId;
    }

    public int getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(int productTypeId) {
        this.productTypeId = productTypeId;
    }
}
