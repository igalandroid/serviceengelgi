package com.ivalue.serviceeng.dao;

import com.ivalue.serviceeng.dto.CheckpointsDTO;

import java.util.ArrayList;

/**
 * Created by Sasikumar on 6/18/2016.
 */
public interface CheckpointsDAO {

    public long create(CheckpointsDTO checkpointsDTO);

    public ArrayList<CheckpointsDTO> findGroupsById(int errorSubTypeId);

    public ArrayList<CheckpointsDTO> findGroupsByCheckPointId(int checkpointId);

    public void deleteTableData();

    public long getCount();
}
