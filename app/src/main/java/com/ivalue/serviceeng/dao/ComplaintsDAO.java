package com.ivalue.serviceeng.dao;

import com.ivalue.serviceeng.dto.ComplaintsDTO;

import java.util.ArrayList;

/**
 * Created by Sasikumar on 6/18/2016.
 */
public interface ComplaintsDAO {

    public long create(ComplaintsDTO complaintsDTO);

    public ArrayList<ComplaintsDTO> findGroupsById(int errorSubTypeId);

    public void deleteTableData();

    public long getCount();
}
