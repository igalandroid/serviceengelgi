package com.ivalue.serviceeng.dao;

import android.content.Context;

import com.ivalue.serviceeng.dao.impl.CheckPointsSqlDAO;
import com.ivalue.serviceeng.dao.impl.ComplaintsDAO;
import com.ivalue.serviceeng.dao.impl.ProductGroupDAO;
import com.ivalue.serviceeng.dto.CheckpointsDTO;
import com.ivalue.serviceeng.dto.ComplaintsDTO;
import com.ivalue.serviceeng.dto.ProductGroupDTO;

/**
 * Created by Sasikumar on 6/6/2016.
 */
public class DAOFactory {

    private Context context;
    public DAOFactory(Context context){
        this.context = context;
    }
    public static synchronized Object createObjectDAO(Context context, Class<? extends Object> classDao)
            throws IllegalArgumentException {

        Object daoFactory = null;
       /* if(classDao.getName().equals(ErrorCodeDTO.class.getName())){
            daoFactory = new ErrorCodeSqlDAO(context);
        }else*/ if(classDao.getName().equals(ProductGroupDTO.class.getName())){
            daoFactory = new ProductGroupDAO(context);
        }else if(classDao.getName().equals(ComplaintsDTO.class.getName())){
            daoFactory = new ComplaintsDAO(context);
        }else if(classDao.getName().equals(CheckpointsDTO.class.getName())){
            daoFactory = new CheckPointsSqlDAO(context);
        }
        return daoFactory;
    }
}
