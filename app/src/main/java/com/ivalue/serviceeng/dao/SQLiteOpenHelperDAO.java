package com.ivalue.serviceeng.dao;

/**
 * Created by Sasikumar on 6/5/2016.
 */

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;


import com.ivalue.serviceeng.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class SQLiteOpenHelperDAO extends SQLiteOpenHelper {

    private static final String TAG = SQLiteOpenHelperDAO.class.getSimpleName();


    private static ThreadLocal<SQLiteDatabase> db = new ThreadLocal<SQLiteDatabase>();
    private static final ThreadLocal<AtomicInteger> dbRefCount = new ThreadLocal<AtomicInteger>() {
        @Override
        protected AtomicInteger initialValue() {
            return new AtomicInteger(0);
        }
    };

    protected Context context;
    private static SQLiteOpenHelperDAO sqlHelper;
    private static int dbCounter = 0;

    public static synchronized SQLiteOpenHelperDAO getInstance(Context context) {

        if (sqlHelper == null) {
            sqlHelper = new SQLiteOpenHelperDAO(context);
        }

        if (dbCounter > 1000) {
            dbCounter = 0;
        } else {
            dbCounter++;
        }
        return sqlHelper;
    }

    public SQLiteOpenHelperDAO(Context context) {
        super(context, context.getString(R.string.db_name), null, Integer.parseInt(context
                .getString(R.string.db_version)));
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(context.getString(R.string.db_create_product_group));
        db.execSQL(context.getString(R.string.db_create_complaints_type));
        db.execSQL(context.getString(R.string.db_create_checkpoints_type));

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
