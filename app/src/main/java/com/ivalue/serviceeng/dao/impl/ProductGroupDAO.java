package com.ivalue.serviceeng.dao.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ivalue.serviceeng.R;
import com.ivalue.serviceeng.dao.SQLiteOpenHelperDAO;
import com.ivalue.serviceeng.dto.ProductGroupDTO;

import java.util.ArrayList;

/**
 * Created by Sasikumar on 6/6/2016.
 */
public class ProductGroupDAO extends SQLiteOpenHelperDAO implements com.ivalue.serviceeng.dao.ProductGroupDAO {

    public ProductGroupDAO(Context context){
        super(context);
    }

    public long create(ProductGroupDTO productGroupDTO) {

        SQLiteDatabase db = null;
        long errorInfoId = 0;
        /*
        id INTEGER PRIMARY KEY,
			error_type TEXT
        */
        try {
            db = SQLiteOpenHelperDAO.getInstance(context).getWritableDatabase();
            db.beginTransaction();

            ContentValues values = new ContentValues();
            values.put("id", productGroupDTO.getProductId());
            values.put("product_type", productGroupDTO.getProductType() );
            errorInfoId = db.insert(context.getString(R.string.tbl_error_type),null,values);
            db.setTransactionSuccessful();

        } catch (SQLiteConstraintException e) {
          Log.d("Error Type create ",""+e.toString());
        }finally {
            if (db != null) {
                db.endTransaction();
            }
        }
        return errorInfoId;
    }

    public ArrayList<ProductGroupDTO> getAllErrorType(){

        SQLiteDatabase db = null;
        ArrayList<ProductGroupDTO> errorSubTypesDTOList=null;
        try {
            /*
             id INTEGER PRIMARY KEY,
                        error_subtype TEXT,
                        error_type_id INTEGER
             */
            String query = "select * from "+context.getString(R.string.tbl_error_type);
            db = SQLiteOpenHelperDAO.getInstance(context).getWritableDatabase();
            db.beginTransaction();
            Cursor cursor = db.rawQuery(query, null);
            errorSubTypesDTOList = new ArrayList<ProductGroupDTO>();
            if(cursor.moveToFirst()){
                do{
                    ProductGroupDTO errorSubTypesDTO = new ProductGroupDTO();
                    errorSubTypesDTO.setProductId(cursor.getInt(0));
                    errorSubTypesDTO.setProductType(cursor.getString(1));

                    errorSubTypesDTOList.add(errorSubTypesDTO);
                }while (cursor.moveToNext());
            }
            db.setTransactionSuccessful();

        }catch(Exception sxc){
            Log.d("getAllErrorType", "" + sxc.toString());
        }finally {
            if (db != null) {
                db.endTransaction();
            }
        }
        return errorSubTypesDTOList;
    }
    public void deleteTableData(){
        SQLiteDatabase db = null;
        try {
            db = SQLiteOpenHelperDAO.getInstance(context).getWritableDatabase();
            db.beginTransaction();
            db.delete(context.getString(R.string.tbl_error_type), null, null);

        } catch (Exception sqlEx) {

        }
    }
    public long getCount(){
        SQLiteDatabase db = null;
        long cnt = 0;
        try {
            db = SQLiteOpenHelperDAO.getInstance(context).getWritableDatabase();
            cnt  = DatabaseUtils.queryNumEntries(db, context.getString(R.string.tbl_error_type));


        }catch(Exception ex){

        }
        return cnt;
    }

}
