package com.ivalue.serviceeng.dao;

import com.ivalue.serviceeng.dto.ProductGroupDTO;

import java.util.ArrayList;

/**
 * Created by Sasikumar on 6/18/2016.
 */
public interface ProductGroupDAO {


    public long create(ProductGroupDTO productGroupDTO);

    public ArrayList<ProductGroupDTO> getAllErrorType();

    public void deleteTableData();

    public long getCount();

}
