package com.ivalue.serviceeng.dao.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ivalue.serviceeng.R;
import com.ivalue.serviceeng.dao.CheckpointsDAO;
import com.ivalue.serviceeng.dao.SQLiteOpenHelperDAO;
import com.ivalue.serviceeng.dto.CheckpointsDTO;

import java.util.ArrayList;

/**
 * Created by Sasikumar on 6/6/2016.
 */
public class CheckPointsSqlDAO extends SQLiteOpenHelperDAO implements CheckpointsDAO {

    public CheckPointsSqlDAO(Context context) {
        super(context);
    }

    public long create(CheckpointsDTO checkpointsDTO) {

        SQLiteDatabase db = null;
        long errorInfoId = -1;
        String fileName = "",imageFileName="";
        try {
            db = SQLiteOpenHelperDAO.getInstance(context).getWritableDatabase();
            db.beginTransaction();

            String filePath = checkpointsDTO.getFilePath();
            if(filePath.length()>0)
            {
                fileName = filePath.substring(filePath.lastIndexOf("/")+1,filePath.length());
            }
            String imageFilePath = checkpointsDTO.getCheckImagePath();
            if(imageFilePath.length()>0){
                imageFileName = imageFilePath.substring(imageFilePath.lastIndexOf("/")+1,imageFilePath.length());
            }

            ContentValues values = new ContentValues();
            values.put("id", checkpointsDTO.getCheckPointCodeId());
            values.put("checkpoint_code", checkpointsDTO.getCheckPointCode());
            values.put("checking_procedure", checkpointsDTO.getCheckingProcedure());
            values.put("checkpoint_solution", checkpointsDTO.getCheckPointSolution());
            values.put("complaint_type_id", checkpointsDTO.getComplaintsTypeId());
            values.put("file_name",fileName);
            values.put("file_path",filePath);
            values.put("checkpoint_probable", checkpointsDTO.getCheckPointProbableRootCause());
            values.put("product_type_id", checkpointsDTO.getProductTypeId());
            values.put("image_name",imageFileName);
            values.put("image_path",imageFilePath);

            errorInfoId = db.insert(context.getString(R.string.tbl_error_codi_type),null,values);
            db.setTransactionSuccessful();
        } catch (SQLiteConstraintException e) {
            Log.d("create error",""+e.toString());
        }finally {
            if (db != null) {
                db.endTransaction();
            }
        }
        return errorInfoId;
    }
    public ArrayList<CheckpointsDTO> findGroupsByCheckPointId(int checkPointId){

        SQLiteDatabase db = null;
        ArrayList<CheckpointsDTO> checkpointsDTOList =null;
        try {


            String query = "select * from "+context.getString(R.string.tbl_error_codi_type)+" where id="+checkPointId;
            Log.d(" findByCheckPointId ", "" + query);
            db = SQLiteOpenHelperDAO.getInstance(context).getWritableDatabase();
            db.beginTransaction();
            Cursor cursor = db.rawQuery(query, null);
            checkpointsDTOList = new ArrayList<CheckpointsDTO>();
            if(cursor.moveToFirst()){
                do{
                    /*
                    id INTEGER,
            checkpoint_code TEXT,
            checking_procedure TEXT,
            checkpoint_solution TEXT,
            file_name TEXT,
            file_path TEXT,
            checkpoint_probable TEXT,
            complaint_type_id INTEGER,
			product_type_id INTEGER
                     */
                    CheckpointsDTO checkpointsDTO = new CheckpointsDTO();

                    checkpointsDTO.setCheckPointCodeId(cursor.getInt(cursor.getColumnIndex("id")));
                    checkpointsDTO.setCheckPointCode(cursor.getString(cursor.getColumnIndex("checkpoint_code")));
                    checkpointsDTO.setCheckingProcedure(cursor.getString(cursor.getColumnIndex("checking_procedure")));
                    checkpointsDTO.setCheckPointSolution(cursor.getString(cursor.getColumnIndex("checkpoint_solution")));
                    checkpointsDTO.setFileName(cursor.getString(cursor.getColumnIndex("file_name")));
                    checkpointsDTO.setCheckPointProbableRootCause(cursor.getString(cursor.getColumnIndex("checkpoint_probable")));
                    checkpointsDTO.setComplaintsTypeId(cursor.getInt(cursor.getColumnIndex("complaint_type_id")));
                    checkpointsDTO.setProductTypeId(cursor.getInt(cursor.getColumnIndex("product_type_id")));
                    checkpointsDTO.setCheckImageName(cursor.getString(cursor.getColumnIndex("image_name")));
                    checkpointsDTOList.add(checkpointsDTO);
                    Log.d("","findGroupsByCheckPointId File Path : "+ checkpointsDTO.getFilePath());
                }while (cursor.moveToNext());
            }
            db.setTransactionSuccessful();

        }catch(Exception sxc){
            Log.d("findGroupsById",""+sxc.toString());
        }finally {
            if (db != null) {
                db.endTransaction();
            }
        }
        return checkpointsDTOList;
    }
    public ArrayList<CheckpointsDTO> findGroupsById(int errorSubTypeId){

        SQLiteDatabase db = null;
        ArrayList<CheckpointsDTO> checkpointsDTOList =null;
        try {


            String query = "select * from "+context.getString(R.string.tbl_error_codi_type)+" where complaint_type_id="+errorSubTypeId;
            Log.d("find by Quer ",""+query);
            db = SQLiteOpenHelperDAO.getInstance(context).getWritableDatabase();
            db.beginTransaction();
            Cursor cursor = db.rawQuery(query, null);
            checkpointsDTOList = new ArrayList<CheckpointsDTO>();
            if(cursor.moveToFirst()){
                do{
                    /*
                    id INTEGER,
            checkpoint_code TEXT,
            checking_procedure TEXT,
            checkpoint_solution TEXT,
            file_name TEXT,
            file_path TEXT,
            checkpoint_probable TEXT,
            complaint_type_id INTEGER,
			product_type_id INTEGER
                     */
                    CheckpointsDTO checkpointsDTO = new CheckpointsDTO();

                    checkpointsDTO.setCheckPointCodeId(cursor.getInt(cursor.getColumnIndex("id")));
                    checkpointsDTO.setCheckPointCode(cursor.getString(cursor.getColumnIndex("checkpoint_code")));
                    checkpointsDTO.setCheckingProcedure(cursor.getString(cursor.getColumnIndex("checking_procedure")));
                    checkpointsDTO.setCheckPointSolution(cursor.getString(cursor.getColumnIndex("checkpoint_solution")));
                    checkpointsDTO.setFileName(cursor.getString(cursor.getColumnIndex("file_name")));
                    checkpointsDTO.setCheckPointProbableRootCause(cursor.getString(cursor.getColumnIndex("checkpoint_probable")));
                    checkpointsDTO.setComplaintsTypeId(cursor.getInt(cursor.getColumnIndex("complaint_type_id")));
                    checkpointsDTO.setProductTypeId(cursor.getInt(cursor.getColumnIndex("product_type_id")));
                    checkpointsDTO.setCheckImageName(cursor.getString(cursor.getColumnIndex("image_name")));
                    checkpointsDTOList.add(checkpointsDTO);
                    Log.d("","findGroupsById : "+ checkpointsDTO.getFilePath()+"----"+checkpointsDTO.getCheckImageName());
                }while (cursor.moveToNext());
            }
            db.setTransactionSuccessful();

        }catch(Exception sxc){
            Log.d("findGroupsById",""+sxc.toString());
        }finally {
            if (db != null) {
                db.endTransaction();
            }
        }
        return checkpointsDTOList;
    }
    public void deleteTableData(){
        SQLiteDatabase db = null;
        try {
            db = SQLiteOpenHelperDAO.getInstance(context).getWritableDatabase();
            db.beginTransaction();
            db.delete(context.getString(R.string.tbl_error_codi_type), null, null);

        } catch (Exception sqlEx) {

        }
    }

    public long getCount(){
        SQLiteDatabase db = null;
        long cnt = 0;
        try {
            db = SQLiteOpenHelperDAO.getInstance(context).getWritableDatabase();
            cnt  = DatabaseUtils.queryNumEntries(db, context.getString(R.string.tbl_error_codi_type));


        }catch(Exception ex){

        }
        return cnt;
    }

}
