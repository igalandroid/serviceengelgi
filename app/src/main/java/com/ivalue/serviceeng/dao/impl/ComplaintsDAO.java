package com.ivalue.serviceeng.dao.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ivalue.serviceeng.R;
import com.ivalue.serviceeng.dao.SQLiteOpenHelperDAO;
import com.ivalue.serviceeng.dto.ComplaintsDTO;

import java.util.ArrayList;

/**
 * Created by Sasikumar on 6/6/2016.
 */
public class ComplaintsDAO extends SQLiteOpenHelperDAO implements com.ivalue.serviceeng.dao.ComplaintsDAO {

    public ComplaintsDAO(Context context){
        super(context);
    }

    public long create(ComplaintsDTO complaintsDTO) {

        SQLiteDatabase db = null;
        long errorInfoId = 0;
            /*
            id INTEGER PRIMARY KEY,
                        error_subtype TEXT,
                        error_type_id INTEGER
             */
        try {
            db = SQLiteOpenHelperDAO.getInstance(context).getWritableDatabase();
            db.beginTransaction();
            ContentValues values = new ContentValues();
            values.put("id", complaintsDTO.getComplaintsTypeId());
            values.put("complaint_type", complaintsDTO.getComplaintsType());
            values.put("product_id", complaintsDTO.getProductId());
            values.put("conf_check_points",complaintsDTO.getConfirmCheckPoint());
            values.put("definition",complaintsDTO.getDefinition());

            errorInfoId = db.insert(context.getString(R.string.tbl_error_sub_type),null,values);
            db.setTransactionSuccessful();

        } catch (SQLiteConstraintException e) {

            Log.d("Error Subtype ",""+e.toString());
        }finally {
            if (db != null) {
                db.endTransaction();
            }
        }
        return errorInfoId;
    }

    public ArrayList<ComplaintsDTO> findGroupsById(int errorTypeId){

        SQLiteDatabase db = null;
        ArrayList<ComplaintsDTO> complaintsDTOList =null;
        try {

            /*
             id INTEGER PRIMARY KEY,
                        error_subtype TEXT,
                        error_type_id INTEGER
             */
            String query = "select * from "+context.getString(R.string.tbl_error_sub_type)+" where product_id="+errorTypeId;
            Log.d("Sub type query ",""+query);
            db = SQLiteOpenHelperDAO.getInstance(context).getWritableDatabase();
            db.beginTransaction();
            Cursor cursor = db.rawQuery(query, null);
            complaintsDTOList = new ArrayList<ComplaintsDTO>();
            if(cursor.moveToFirst()){
                do{
                    ComplaintsDTO complaintsDTO = new ComplaintsDTO();

                    complaintsDTO.setComplaintsTypeId(cursor.getInt(cursor.getColumnIndex("id")));
                    Log.d("", "" + "sub id " + complaintsDTO.getComplaintsTypeId());

                    complaintsDTO.setComplaintsType(cursor.getString(cursor.getColumnIndex("complaint_type")));
                    Log.d("", "Sub type " + complaintsDTO.getComplaintsType());

                    complaintsDTO.setProductId(cursor.getInt(cursor.getColumnIndex("product_id")));
                    Log.d("", "" + "type id  " + complaintsDTO.getProductId());

                    complaintsDTO.setConfirmCheckPoint(cursor.getString(cursor.getColumnIndex("conf_check_points")));

                    complaintsDTO.setDefinition(cursor.getString(cursor.getColumnIndex("definition")));

                    complaintsDTOList.add(complaintsDTO);

                }while (cursor.moveToNext());
            }
            db.setTransactionSuccessful();

        }catch(Exception sxc){
            Log.d("findGroupsById", "" + sxc.toString());
        }finally {
            if (db != null) {
                db.endTransaction();
            }
        }
        return complaintsDTOList;
    }
    public void deleteTableData(){
        SQLiteDatabase db = null;
        try {
            db = SQLiteOpenHelperDAO.getInstance(context).getWritableDatabase();
            db.beginTransaction();
            db.delete(context.getString(R.string.tbl_error_sub_type), null, null);

        } catch (Exception sqlEx) {

        }
    }
    public long getCount(){
        SQLiteDatabase db = null;
        long cnt = 0;
        try {
            db = SQLiteOpenHelperDAO.getInstance(context).getWritableDatabase();
            cnt  = DatabaseUtils.queryNumEntries(db, context.getString(R.string.tbl_error_sub_type));

        }catch(Exception ex){

        }
        return cnt;
    }

}
