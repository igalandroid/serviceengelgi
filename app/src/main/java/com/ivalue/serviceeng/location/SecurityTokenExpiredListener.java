package com.ivalue.serviceeng.location;

/**
 * Created by Sasikumar on 9/16/2016.
 */

        import android.content.BroadcastReceiver;
        import android.content.Context;
        import android.content.Intent;
        import android.widget.Toast;


        import com.ivalue.serviceeng.bean.ThreadDefine;
import com.ivalue.serviceeng.ui.LoginActivity;
        import com.ivalue.serviceeng.ui.SecurityTokenExpiredAlertActivity;

public class SecurityTokenExpiredListener extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        //Toast.makeText(context,"Start SecurityTokenExpiredListener ",Toast.LENGTH_SHORT).show();
        Intent logoutInt = new Intent(context,SecurityTokenExpiredAlertActivity.class);
        logoutInt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(logoutInt);

    }
}