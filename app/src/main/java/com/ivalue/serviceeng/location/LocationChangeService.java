package com.ivalue.serviceeng.location;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;

import com.ivalue.serviceeng.R;
import com.ivalue.serviceeng.bean.ThreadDefine;
import com.ivalue.serviceeng.bean.Tracker;
import com.ivalue.serviceeng.util.ConnectionDetector;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by
 */
public class LocationChangeService extends Service {

    public LocationManager locationManager;
    public MyLocationListener listener;
    public Location previousBestLocation = null;

    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false, canGetLocation = false;


    double latitude;
    double longitude;

    private Context context;

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1000; // 10 meters

    private  long MIN_TIME_BW_UPDATES = 5000 * 60 * 1; // 1 minute

    private Resources resources;

    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();

        resources = getResources();

        MIN_TIME_BW_UPDATES = 5000 * 60 * Integer.parseInt(resources.getString(R.string.location_send_interval));

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

        scheduler.scheduleAtFixedRate(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub

                getLocation();

            }
        }, 0, 2, TimeUnit.MINUTES);

        locationManager.addGpsStatusListener(new GpsStatus.Listener() {
            @Override
            public void onGpsStatusChanged(int event) {

                // Log.d("onGpsStatusChanged","GPS AVAILABLE "+event);

                switch (event) {

                    case LocationProvider.AVAILABLE:

                        getLocation();

                        break;
                }

            }
        });

        listener = new MyLocationListener();

        getLocation();

        Log.d("--LocationChangeSer ->", "--->");


    }

    @Override
    public void onStart(Intent intent, int startId) {
      /*  locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        listener = new MyLocationListener();
       // locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,2000,0, listener);
       // locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 0, listener);

        getLocation();*/
    }

    public Location getLocation() {

        try {
            locationManager = (LocationManager) getApplicationContext()
                    .getSystemService(Context.LOCATION_SERVICE);

            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);


            //locationManager.addGpsStatusListener(listener);

            if (!isGPSEnabled && !isNetworkEnabled) {
                Log.d("GPS is not enabled---->", "---------->");
                //  Log.d("GPS is not enabled---->","---------->");
                // Log.d("GPS is not enabled---->","---------->");
            } else {
                this.canGetLocation = true;
                if (isNetworkEnabled) {

                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, listener);

                    Log.d("Network", "Network Enabled");
                    if (locationManager != null) {
                        previousBestLocation = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (previousBestLocation != null) {
                            latitude = previousBestLocation.getLatitude();
                            longitude = previousBestLocation.getLongitude();

                            storeCitylocation(previousBestLocation);
                        }
                    }
                }
                if (isGPSEnabled) {
                    if (previousBestLocation == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, listener);
                        Log.d("GPS", "GPS Enabled");
                        if (locationManager != null) {
                            previousBestLocation = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (previousBestLocation != null) {
                                latitude = previousBestLocation.getLatitude();
                                longitude = previousBestLocation.getLongitude();

                                storeCitylocation(previousBestLocation);
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return previousBestLocation;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > MIN_TIME_BW_UPDATES;
        boolean isSignificantlyOlder = timeDelta < -MIN_TIME_BW_UPDATES;
        boolean isNewer = timeDelta > 0;


        if (isSignificantlyNewer) {
            return true;

        } else if (isSignificantlyOlder) {
            return false;
        }

        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }


    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }


    @Override
    public void onDestroy() {
        // handler.removeCallbacks(sendUpdatesToUI);
        super.onDestroy();
        Log.v("STOP_SERVICE", "DONE");
        locationManager.removeUpdates(listener);
    }


    // Store the Location in shared preference and Tracker
    private void storeCitylocation(Location loc) {

        try {

            double latitude = loc.getLatitude();

            double longitude = loc.getLongitude();

            Tracker.USER_LATITUDE = String.valueOf(latitude);

            Tracker.USER_LONGITUDE = String.valueOf(longitude);

            new LocationAsyncTask().execute(1);

            /*Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

            List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);

            if (addressList != null && addressList.size() > 0) {

                Address address = addressList.get(0);

                Tracker.USER_LOCATION = address.getLocality();

                *//*if (Tracker.USER_LOCATION == null)
                    Tracker.USER_LOCATION = getResources().getString(R.string.default_location);*//*

                Log.d("Current Location" + address.getLocality(), "---" + address.getCountryCode());
            }*/

        } catch (Exception ex) {

            Log.d("Exception in GEO--" + ex.toString(), "");

        }
    }

    private class LocationAsyncTask extends AsyncTask<Integer, Integer, Integer>

    {
        int result = 0;

        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected Integer doInBackground(Integer... params) {

                try {
                    OkHttpClient client = new OkHttpClient();

                    MediaType JSON
                            = MediaType.parse("application/json; charset=utf-8");

                    Request request = null;

                    if(ThreadDefine.AUTHENTICATION_TOKEN.length()>0 && new ConnectionDetector(context).isConnectingToInternet()) {

                    /*

                        final_ws_se_loc/get_se_loc.svc/se/location/Token=e475e1e6-2428-4bbb-47c6-c99336a9&Long=80.2508246&Lati=13.0524139

                    */

                        String locationUrl = resources.getString(R.string.app_url) +"location/Token="+ThreadDefine.AUTHENTICATION_TOKEN+"&Long=" +Tracker.USER_LONGITUDE+"&Lati="+ Tracker.USER_LATITUDE ;

                        Log.d("","Location url ---> \n"+locationUrl);

                        request = new Request.Builder()
                                .url(locationUrl)
                                .build();

                        Response response = client.newCall(request).execute();

                        JSONObject responseJsonObj = new JSONObject(response.body().string());

                        if(responseJsonObj.has("Status")){

                            String successResp =  responseJsonObj.getString("Status");

                            if(successResp.equalsIgnoreCase("SUCCESS"))
                            {

                                Log.d("","Location Success--------");

                            }else {

                                Log.d("","Location Fail--------");

                            }
                        }


                    }

                }catch(Exception ex){


                    Log.d("Location Send ----"+ex.toString(),"----");
                }


            return result;

        }

        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);


        }

    }


    public class MyLocationListener implements LocationListener {

        public void onLocationChanged(final Location loc) {

            if (isBetterLocation(loc, previousBestLocation)) {

                storeCitylocation(loc);

            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            //Toast.makeText(getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT).show();

            // getLocation();
        }

        @Override
        public void onProviderEnabled(String provider) {
            // Toast.makeText( getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();

            // Log.d("---GPS Enable","GPS Enable "+provider);

            getLocation();

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

            //  Log.d("---GPS AVAILABLE --->"+status,"GPS AVAILABLE "+provider);


            switch (status) {



                case LocationProvider.AVAILABLE:

                    getLocation();

                    break;
            }
        }

    }
}