package com.ivalue.serviceeng.util;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import  com.ivalue.serviceeng.R;


public class UiUtils {

    //Get the Device Width
    public static int getDeviceWidth(Activity activity) {

        DisplayMetrics displaymetrics = new DisplayMetrics();

        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        return displaymetrics.widthPixels;
    }

    //Get Device Height
    public static int getDeviceHeight(Activity activity) {

        DisplayMetrics displaymetrics = new DisplayMetrics();

        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        return displaymetrics.heightPixels;
    }

    //Hide the keypad if visible
    public static void hideKeypad(Activity activity) {

        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

        //inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);


        inputManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

    }

    public static void actionBarTitle(ActionBar actionBar, Activity thisActivity,String titleStr) {

        if (actionBar != null){

            LayoutInflater inflator = (LayoutInflater) thisActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = inflator.inflate(R.layout.action_bar_title, null);

            TextView titleActionBarTxt = (TextView) view.findViewById(R.id.title);

            titleActionBarTxt.setText(titleStr);

            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

            actionBar.setCustomView(view, new ActionBar.LayoutParams(
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    ActionBar.LayoutParams.MATCH_PARENT,
                    Gravity.CENTER
            ));

            actionBar.setDisplayHomeAsUpEnabled(true);

            actionBar.setHomeButtonEnabled(true);

            actionBar.setDisplayShowTitleEnabled(false);

            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

   /* public static void centerActionBarTitle(Activity activity) {
        try {

            int titleId = 0;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                titleId = activity.getResources().getIdentifier("action_bar_title","id", "android");
            } else {

                titleId = R.id.action_bar_title;
            }

            if (titleId > 0) {
                TextView titleTextView = (TextView) activity.findViewById(titleId);

                DisplayMetrics metrics = activity.getResources().getDisplayMetrics();

                LinearLayout.LayoutParams txvPars = (LinearLayout.LayoutParams) titleTextView
                        .getLayoutParams();
                txvPars.gravity = Gravity.CENTER_HORIZONTAL;
                txvPars.width = metrics.widthPixels;
                titleTextView.setLayoutParams(txvPars);
               // titleTextView.setGravity(Gravity.CENTER);

                //titleTextView.setTextColor(getResources().getColor(R.color.orange_color));

                titleTextView.setEllipsize(TextUtils.TruncateAt.END);

                titleTextView.setPadding(0, 0, 0, 0);


            }
        } catch (Exception ex) {

            Log.e("Exception " + ex.toString(), "--->" + ex.toString());
        }
    }*/
}
