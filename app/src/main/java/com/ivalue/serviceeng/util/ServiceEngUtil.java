package com.ivalue.serviceeng.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 * Created by Sasikumar on 7/16/2016.
 */
public class ServiceEngUtil {

    public static PackageInfo getCurrentVersion(Context context) {
        PackageInfo serviceEngInfo;
        try {
            serviceEngInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            serviceEngInfo = null;
        }
        return serviceEngInfo;
    }
}
