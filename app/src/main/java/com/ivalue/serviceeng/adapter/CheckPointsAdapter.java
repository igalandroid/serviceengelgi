package com.ivalue.serviceeng.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ivalue.serviceeng.R;
import com.ivalue.serviceeng.dto.CheckpointsDTO;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Sasikumar on 5/18/2016.
 */
public class CheckPointsAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;

    private Context context;

    private ArrayList<CheckpointsDTO> errorSubTypesDTOArrayList;

    public CheckPointsAdapter(Context context, ArrayList<CheckpointsDTO> errorSubTypesDTOArrayList){

        this.context = context;

        this.errorSubTypesDTOArrayList = errorSubTypesDTOArrayList;

        layoutInflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {

        return errorSubTypesDTOArrayList.size();
    }

    @Override
    public Object getItem(int position) {

        return errorSubTypesDTOArrayList.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        try {

            ViewHolder viewHolder = null;

            final String imagefileName;

            if(convertView==null){

                convertView = layoutInflater.inflate(R.layout.checkpoints_type_single_row,null);

                viewHolder = new ViewHolder();

                viewHolder.newsLetterTxt = (TextView) convertView.findViewById(R.id.error_type_head);

               viewHolder.imageIcon = (ImageView) convertView.findViewById(R.id.ico_img);

                convertView.setTag(viewHolder);
            }else{

                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.newsLetterTxt.setText("" + errorSubTypesDTOArrayList.get(position).getCheckPointCode());

            imagefileName = errorSubTypesDTOArrayList.get(position).getCheckImageName();
            if(imagefileName.length()>0) {

                viewHolder.imageIcon.setVisibility(View.VISIBLE);

                if (!isImageFileExists(imagefileName))
                    viewHolder.imageIcon.setVisibility(View.GONE);
                viewHolder.imageIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        loadPhoto(imagefileName);
                    }
                });
            }else
                viewHolder.imageIcon.setVisibility(View.GONE);


        }catch(Exception ex){

            Log.d("Recieve Adapter", "" + ex.toString());
        }

        return convertView;
    }

    public static class ViewHolder {

        TextView newsLetterTxt;
        ImageView imageIcon;

    }

    private void loadPhoto(String pdfFileName){

        File imgFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+ context.getString(R.string.folder_downloads)+pdfFileName);
        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        ImageView imageView = new ImageView(context);
        imageView.setImageBitmap(myBitmap);


        AlertDialog.Builder imageDialog = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.popup_image_view,null);
        ImageView image = (ImageView) layout.findViewById(R.id.fullimage);
        image.setImageDrawable(imageView.getDrawable());
        //ImageView closeImgview = (ImageView) layout.findViewById(R.id.popup_close_img);




        imageDialog.setView(layout);
        imageDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create().show();
    }
    private void openImage(String pdfFileName){
        File imgFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+ context.getString(R.string.folder_downloads)+pdfFileName);

       final Dialog builder = new Dialog(context,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        //builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setCancelable(true);
        builder.setCanceledOnTouchOutside(true);
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                builder.dismiss();
            }
        });

        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        ImageView imageView = new ImageView(context);
        imageView.setImageBitmap(myBitmap);
        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        builder.show();

    }

    private boolean isImageFileExists(String pdfFileName){
        try {
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + context.getString(R.string.folder_downloads)  + pdfFileName);
            if (file.exists())
                return true;

        }catch (Exception ex){
            return false;
        }
        return false;
    }
}