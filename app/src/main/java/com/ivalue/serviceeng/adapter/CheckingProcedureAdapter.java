package com.ivalue.serviceeng.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ivalue.serviceeng.R;
import com.ivalue.serviceeng.dto.CheckpointsDTO;


import java.io.File;
import java.util.ArrayList;

/**
 * Created by Sasikumar on 5/18/2016.
 */
public class CheckingProcedureAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;

    private Context context;

    private ArrayList<CheckpointsDTO> errorcodificationDTOArrayList;

    public CheckingProcedureAdapter(Context context, ArrayList<CheckpointsDTO> errorcodificationDTOArrayList){

        this.context = context;

        this.errorcodificationDTOArrayList = errorcodificationDTOArrayList;

        layoutInflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return errorcodificationDTOArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return errorcodificationDTOArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        try {

            ViewHolder viewHolder = null;

            final String pdfFileName;

            if(convertView==null){

                convertView = layoutInflater.inflate(R.layout.error_codification_single_row,null);

                viewHolder = new ViewHolder();

                viewHolder.solutionTxt = (TextView) convertView.findViewById(R.id.error_code_solution_txt);

                viewHolder.checkProcTxt =  (TextView) convertView.findViewById(R.id.check_procedure_txt);

                viewHolder.probableRootCauseTxt = (TextView) convertView.findViewById(R.id.error_probable_cause);

                viewHolder.pdfImageView = (ImageView) convertView.findViewById(R.id.pdf_imgview);

                convertView.setTag(viewHolder);
            }else{
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.solutionTxt.setText(""+errorcodificationDTOArrayList.get(position).getCheckPointSolution());

            viewHolder.probableRootCauseTxt.setText(""+errorcodificationDTOArrayList.get(position).getCheckPointProbableRootCause());

            viewHolder.checkProcTxt.setText(""+errorcodificationDTOArrayList.get(position).getCheckingProcedure());

            pdfFileName = errorcodificationDTOArrayList.get(position).getFileName();
            Log.d("File Path",""+pdfFileName);
           if(pdfFileName.length()>0) {
               //pdfFileName = pdfFilePath.substring(pdfFilePath.lastIndexOf("/"));
               if (!isPdfFileExists(pdfFileName))
                   viewHolder.pdfImageView.setVisibility(View.GONE);

                 viewHolder.pdfImageView.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {

                       Log.d("Pdf File name ",""+pdfFileName);
                       openPdf(pdfFileName);
                   }
               });
           }else
               viewHolder.pdfImageView.setVisibility(View.GONE);
        }catch(Exception ex){
            Log.d("Error codification Adap", "" + ex.toString());
        }
        return convertView;
    }

    public static class ViewHolder {

        TextView checkProcTxt,probableRootCauseTxt,solutionTxt;
        ImageView pdfImageView;

    }

    private void openPdf(String pdfFileName){
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+ context.getString(R.string.folder_downloads)+pdfFileName);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file),"application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        context.startActivity(intent);
    }

    private boolean isPdfFileExists(String pdfFileName){
      try {
          File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + context.getString(R.string.folder_downloads)  + pdfFileName);
          if (file.exists())
              return true;

      }catch (Exception ex){
          return false;
      }
        return false;
    }

}