package com.ivalue.serviceeng.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ivalue.serviceeng.R;
import com.ivalue.serviceeng.dto.ProductGroupDTO;

import java.util.ArrayList;

/**
 * Created by Sasikumar on 5/18/2016.
 */
public class ProductGroupAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;

    private Context context;

    private ArrayList<ProductGroupDTO> newsLetterDTOArrayList;

    public ProductGroupAdapter(Context context, ArrayList<ProductGroupDTO> newsLetterDTOArrayList){

        this.context = context;

        this.newsLetterDTOArrayList = newsLetterDTOArrayList;

        layoutInflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {

        return newsLetterDTOArrayList.size();
    }

    @Override
    public Object getItem(int position) {

        return newsLetterDTOArrayList.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {

            ViewHolder viewHolder = null;

            if(convertView==null){

                convertView = layoutInflater.inflate(R.layout.error_type_single_row,null);

                viewHolder = new ViewHolder();

                viewHolder.newsLetterTxt = (TextView) convertView.findViewById(R.id.error_type_head);

                convertView.setTag(viewHolder);
            }else{
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.newsLetterTxt.setText(""+newsLetterDTOArrayList.get(position).getProductType());

            //viewHolder.newsLetterTitle.setVisibility(View.GONE);

           // viewHolder.newsLetterDate.setVisibility(View.GONE);

          }catch(Exception ex){

            Log.d("Recieve Adapter", "" + ex.toString());
        }


        return convertView;
    }

    public static class ViewHolder {

        TextView newsLetterTxt;//,newsLetterTitle,newsLetterDate;

    }

}
