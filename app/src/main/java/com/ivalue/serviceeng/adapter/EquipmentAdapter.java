package com.ivalue.serviceeng.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ivalue.serviceeng.R;
 
import com.ivalue.serviceeng.dto.EquipmentDTO;

import java.util.ArrayList;

/**
 * Created by on 24/8/15.
 */
public class EquipmentAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;

    private Context context;

    private ArrayList<EquipmentDTO> equipmentDTOArrayList;

    public EquipmentAdapter(Context context, ArrayList<EquipmentDTO> equipmentDTOArrayList){

        this.context = context;

        this.equipmentDTOArrayList = equipmentDTOArrayList;

        layoutInflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {

        return equipmentDTOArrayList.size();
    }

    @Override
    public Object getItem(int position) {

        return equipmentDTOArrayList.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        try {

            ViewHolder viewHolder = null;

            if(convertView==null){

                convertView = layoutInflater.inflate(R.layout.equipment_single_row,null);

                viewHolder = new ViewHolder();

                viewHolder.equipmentTxt = (TextView) convertView.findViewById(R.id.equipment_name_txt);

                convertView.setTag(viewHolder);

            }else{

                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.equipmentTxt.setText(" "+equipmentDTOArrayList.get(position).getEquipmentName());




        }catch(Exception ex){

            Log.d("Equip Adapter", "" + ex.toString());
        }


        return convertView;
    }

    public static class ViewHolder {

        TextView equipmentTxt;

    }

}
