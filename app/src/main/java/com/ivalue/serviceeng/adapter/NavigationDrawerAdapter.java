package com.ivalue.serviceeng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ivalue.serviceeng.R;


/**
 * Created by railsfactory on 12/7/15.
 */
public class NavigationDrawerAdapter extends ArrayAdapter {


    private LayoutInflater layoutInflater;

    private Context context;

    private String[] listData;

    private int viewResourceId;

    int[] imageResId;

    public NavigationDrawerAdapter(Context context, int viewResourceId, String[] listData,int[] imageResId) {
        super(context, viewResourceId);

        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
        this.context = context;

        this.viewResourceId = viewResourceId;

        this.imageResId = imageResId;
    }

    @Override
    public int getCount() {

        return listData.length;
    }


    @Override
    public int getViewTypeCount() {
        return listData.length;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;

        ViewHolder viewHolder = null;

        if(convertView==null){

            convertView = layoutInflater.inflate(viewResourceId,null);

            viewHolder= new ViewHolder();

            viewHolder.titleTxt = (TextView) convertView.findViewById(R.id.navigation_row_title);

            viewHolder.wholeRow = (RelativeLayout) convertView.findViewById(R.id.whole_row);

            viewHolder.navImg = (ImageView) convertView.findViewById(R.id.navigation_row_img);

            convertView.setTag(viewHolder);

        }else {

            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.titleTxt.setText(listData[position]);

        viewHolder.navImg.setImageResource(imageResId[position]);

        return convertView;
    }

    public static class ViewHolder{

        TextView titleTxt;

        ImageView navImg;

        RelativeLayout wholeRow;


    }
}
